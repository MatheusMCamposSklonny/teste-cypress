const locators = {

    LOGIN: {
        USER: '#Email',
        PASSWORD: '#Senha',
        BTN_LOGIN: '#lnkLogin'
    },

    MODULES: {
        EAUDITOR: '.eauditor > .flipper > .front > .btn',
        ESIMULADOR: '.esimulador > .flipper > .front > .btn',
        EDRIVE: '.edrive > .flipper > .front > .btn',
        ECONSULTA: '.econsulta > .flipper > .front > .btn',
        ERECUPERADOR: '.erecuperador > .flipper > .front > .btn',

        EMONITOR: '.emonitor > .flipper > .front > .btn',
        ECAPTURADOR: '.e360 > .flipper > .front > .btn',
        EATENDIMENTO: '.eatendimento > .flipper > .front > .btn',
    },

    MENU_ERECUPERADOR: {

        SIMPLES_NACIONAL: ':nth-child(2) > .react-card-flipper > .react-card-front >>>>',
        MSG_SIMPLES_NACIONAL: ':nth-child(1) > .react-card-flipper > .react-card-front > .sc-crrsfI > .sc-iBPRYJ > .sc-pFZIQ',
        LUCRO_REAL: ':nth-child(2) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn',
        GERENCIADOR_PROPOSTAS: ':nth-child(3) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn',
        USER: ':nth-child(10) > .fas',
        EMAIL: '.body__caption___email',
        SAIR_CONTA: '.submenu__profile___footer > .ant-btn',
    },

    MENU_ERECUPERADOR_SIMPLES_NACIONAL: {
        RECUPERACAO_PIS: ':nth-child(1) > .react-card-flipper > .react-card-front >>>>',
        RECUPERACAO_PIS_P: ':nth-child(1) > .react-card-flipper > .react-card-front > .sc-crzoAE > .text-center > .ant-btn',
        SEGRECACAO_RECEITAS: ':nth-child(2) > .react-card-flipper > .react-card-front >>>>',
        SEGRECACAO_RECEITAS_P: ':nth-child(2) > .react-card-flipper > .react-card-front > .sc-crzoAE > .text-center > .ant-btn',
        RECUPERACAO_DIFAL: ':nth-child(3) > .react-card-flipper > .react-card-front > .sc-crrsfI > .text-center > .ant-btn'
    },

    ERECUPERADOR_BOTOES: {

        MENU: {
            NOVO: '.sidebar__actions > :nth-child(3)',
            TODAS: '[href="/recuperacao"]',
            LISTAR: '.sidebar__actions > :nth-child(4)',
            CLOSE: '.sidebar__actions > .anticon',
            PRIMEIRO_REGISTRO: ':nth-child(1) > .item__action > .item__action___header',
        },

        CADASTRO: {
            CADASTRAR: '.ant-modal-footer > .ant-btn-primary',
            CANCELAR: '.ant-btn-default',
        },

        PRIMEIRO_CADASTRAR: '.ant-empty-footer > .ant-btn',

        INFORMACOES: {
            DADOS: {
                SALVAR: '.ml-2',
                APAGAR_EMPRESA: '.products__data-company___footer > .ant-btn-danger',
                CONFIRMAR_APAGAR: '.ant-modal-footer > div > .ant-btn-primary',
            }
        },

        FISCAL: {
            ADD_NEW: '.ant-empty-footer > .ant-btn',
            ADD: '.btn-new-product',
            CHECKBOX_ALL: '.ant-table-fixed-left > .ant-table-header > .ant-table-fixed > .ant-table-thead > tr > .ant-table-selection-column > .ant-table-header-column > :nth-child(1) > .ant-table-column-title > .ant-table-selection > .ant-table-selection-select-all-custom > .ant-checkbox > .ant-checkbox-input',
            CHECKBOX: '.ant-table-body-inner > .ant-table-fixed > .ant-table-tbody .ant-table-selection-column > :nth-child(1) > .ant-checkbox-wrapper > .ant-checkbox > .ant-checkbox-input',
            EDITAR: '.products__actions___filters > :nth-child(5)',
            DUPLICADO: '#sDuplicado',
            NAO_CLASSIFICADOS: '.products__actions___filters > :nth-child(1)',
            CLASSIFICADOS: '.products__actions___filters > :nth-child(2)',
            REGISTRO_VAZIO: '.ant-empty',


            CLASSIFICACAO: {
                MANUAL: '.products__actions > :nth-child(2) > :nth-child(2)',
                EDITAR: '.products__actions > :nth-child(2) > :nth-child(2)',
                ECONSULTA: '.products__actions > :nth-child(2) > :nth-child(1)',
                ECONSULTA_OP: {
                    SIM: '.ant-popover-buttons > .ant-btn-primary',
                    NAO: '.ant-popover-buttons > :nth-child(1)',
                },

                CONSIDERAR: '.ant-switch-inner',



                TIPO: '.ant-form-item-children > .ant-select--custom > .ant-select-selection > .ant-select-selection__rendered',
                TIPO_DROPDOWN: '.ant-select-dropdown-menu',
                DATA_INICIAL: '.ant-calendar-picker-input:eq(0)',
                DATA_FINAL: '.ant-calendar-picker-input:eq(1)',
                APAGAR_CLASSIFICACAO: '.ant-col-3 > .mr-2',
                MAIS_CLASSIFICACOES: '.ant-btn-default',
                CLASSIFICAR: '.ant-drawer__footer > .ant-btn',

                X: '.ant-drawer-close > .anticon ',


            },



            APAGAR: {
                BOTAO: '.btn-hover-danger',
                SIM: '.ant-modal-confirm-btns > .ant-btn-danger',
                CANCELAR: '.ant-modal-confirm-btns > :nth-child(1)',
            },

            FILTRO: {
                BOTAO_VAZIO: '.ant-empty-footer > .ant-btn',
                BOTAO: ':nth-child(2) > .ant-btn-icon-only',
                DESCRICAO: '.ant-col-24 > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                CODIGO: ':nth-child(2) > :nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                NCM: ':nth-child(2) > :nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .form-control',
                EX: '.ant-input-group > :input',
                EX_VAZIO: '.ant-input-group > .ant-select > .ant-select-selection',
                DROP: '.ant-select-dropdown-menu-item',
                EAN: ':nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                CONSULTA: '.ant-form-item-children > .ant-select--custom > .ant-select-selection',
                DATA: '.ant-calendar-picker-input',
                LIMPAR: '.mr-3',
                FILTRAR: '.ant-drawer__footer > .ant-btn-primary',
                FILTRO_VAZIO: '.btn',

                TELA_VAZIA: '.ant-tabs-tabpane-active > .ant-empty',
                X: '.ant-drawer-close > .anticon > svg',
            }
        },
    },

    ERECUPERADOR_RESULTADO: {
        BTN_CALSSIFICAR: '.btn-warning',
        BTN_CALSSIFICAR_SEG: '.ant-table-body-inner > .ant-table-fixed > .ant-table-tbody > .ant-table-row > .ant-table-row-cell-break-word > .ant-btn-group > .btn-warning',
        BTN_CALSSIFICAR_SEG_2: '.ant-table-body-inner > .ant-table-fixed > .ant-table-tbody > .ant-table-row > .ant-table-row-cell-break-word > .ant-btn-group > .btn-warning',
        BTN_VISUALIZAR: ':nth-child(7) > .ant-btn-group > .ant-btn-primary',
        BTN_VISUALIZAR_SEG: '.ant-table-body-inner > .ant-table-fixed > .ant-table-tbody > .ant-table-row > .ant-table-row-cell-break-word > .ant-btn-group > .ant-btn-primary',
        BTN_REPROCESSAR: '.ant-btn-group > :nth-child(2)',
        SEG_PIS: '.ant-modal-confirm-btns > :nth-child(1)',
        SEG_ICMS: '.ant-btn-default',
        BTN_APAGAR: {
            EXCLUIR: '.ant-btn-group > .ant-btn-danger',
            OK: '.ant-popover-buttons > .ant-btn-primary',
            CANCELAR: '.ant-popover-buttons > :nth-child(1)',
        },

        BTN_FILTRAR: '.ant-input-suffix > .anticon > svg',
        MODAL_CLASSIFICAR: {
            TITULO: '.ant-modal-body > .row > :nth-child(1) > :nth-child(1)',
            CANCELAR: '.ant-modal-footer > .ant-btn-danger',
            CORRIGIR: '.ant-modal-footer > .ant-btn-primary',
            PROSSEGUIR: '.ant-btn-btn-ghost',
        },
        VAZIO: ':nth-child(4) > .ant-empty',
        EXPORTAR: {
            PDF: ':nth-child(3) > :nth-child(4) > :nth-child(2) > :nth-child(1) > :nth-child(1)',
            EXCEL: ':nth-child(4) > :nth-child(2) > :nth-child(1) > :nth-child(2)',
        },
        FILTRO: '.ant-input-search > .ant-input',

    },

    ERECUPERADOR_ARQUIVO: {
        NOME_STATUS: '.ant-list-item-meta-title',
        STATUS_ATUAL: ':nth-child(1) > [style="width: 150px; text-align: center;"] > .ant-tag',
        DRAG_N_DROP: '.ant-upload-drag-container',
        BTN_ENVIAR: '.sc-cxNHIi',
        NOME_ARQUIVO: '.ant-upload-list-item-name',
        DOWN_MOD_PLAN_PROD: '.mb-2',
        DOWN_MOD_PLAN_REV: '.mb-3:eq(1)',
        SINO: {
            NOTIFICAÇÃO_1: '.ant-list-items > :nth-child(1) > .anticon > svg',
            MODAL: '.ant-modal-body',
            ABAS: '.ant-modal-body > .ant-tabs > .ant-tabs-bar > .ant-tabs-nav-container > .ant-tabs-nav-wrap > .ant-tabs-nav-scroll > .ant-tabs-nav > :nth-child(1) >',
            MENSAGEM: '.ant-tabs-tabpane-active > .ant-list > .ant-spin-nested-loading > .ant-spin-container > .ant-list-items > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content > .ant-list-item-meta-title',
            MENSAGEM: '.ant-tabs-tabpane-active > .ant-list > .ant-spin-nested-loading > .ant-spin-container > .ant-list-items > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content > .ant-list-item-meta-title',
            NOME_ARQUIVO: '.ant-tabs-tabpane-active > .ant-list > .ant-spin-nested-loading > .ant-spin-container > .ant-list-items > .ant-list-item > .ant-list-item-meta > .ant-list-item-meta-content > .ant-list-item-meta-description',
            CLOSE: '.ant-modal-close-x ',

            CONFIGURAR: '.ant-list-item > .ant-btn',

            MODAL_CONFIG: {
                TXT: '.ant-modal-confirm-content',
                SIM: '.ant-modal-confirm-btns > .ant-btn-primary',
                NAO: '.ant-modal-confirm-btns > :nth-child(1)',
            }
        }
    },

    ERECUPERADOR_CAMPOS: {
        CADASTRO: {
            TXT_FILTRO: '.sidebar__body > .ant-input',
            //TXT_NOME: '.row > :nth-child(1) > .ant-input',
            TXT_NOME: '#formCompany > .row > :nth-child(1) > .ant-input',
            NUM_IE: ':nth-child(3) > .form-control',
            NUM_CNPJ: '#formCompany > .row > :nth-child(2) > .form-control',
            BOX_UF: ':nth-child(4) > .ant-select--custom > .ant-select-selection',
            DROPDOWN_UF: '.ant-select-dropdown-menu-item',
        },

        INFORMACOES: {
            DADOS: {
                TXT_RAZAO: '#RazaoSocial',
                NUM_CNPJ: '#Cnpj',
                NUM_INSC_ESTADUAL: '#Ie',
                TXT_RUA: '#Rua',
                TXT_BAIRRO: '#Bairro',
                TXT_MUNICIPIO: '#Municipio',
                BOX_UF: '.ant-select-selection',
                DROPDOWN_UF: '.ant-select-dropdown-menu-item',
                NUM_NUMERO: '#Numero',
                TXT_COMPLEMENTO: '#Complemento',
                NUM_CEP: '#Cep',
            },
            CFOP: {
                PRIMEIRA_RECIETA: '[data-row-key] > :eq(2)',
                BOX_PRIMEIRO_ICMS: '.ant-select-selection__rendered > :eq(2)',
                DROP_DOWN: '.ant-select-dropdown-menu',

                TABELA: '.ant-table-thead > :eq(0)',
                COLUMN_CODIGO: '[data-row-key] > :nth-child(1)',
                COLUMN_RECEITA: '[data-row-key] > :nth-child(3)',
                COLUMN_ICMS: '[data-row-key] > :nth-child(4)',


                SORT: {
                    CODIGO: '.ant-table-column-sorters > :eq(0)',
                    RECEITA: '.ant-table-column-sorters > :eq(2)',
                    ICMS: '.ant-table-column-sorters > :eq(4)',
                },


                FILTRO: {
                    ABRIR: '.d-flex > .ant-btn',
                    ABRIR_VAZIO: '.ant-empty-footer > .ant-btn',
                    FECHAR: '.ant-drawer-close > .anticon',
                    TELA_FILTRO: '.ant-drawer-body',
                    NUM_CODIGO: '.ant-form-item-children > .ant-input',
                    BOX_RECEITA: '.ant-form-item-children > .ant-select--custom',
                    DROPDOWN_RECEITA_SIM: '#d6404671-e7be-48fe-c03b-da2db1058520 > .ant-select-dropdown-menu > :nth-child(2)',
                    DROPDOWN_RECEITA_NAO: '#d6404671-e7be-48fe-c03b-da2db1058520 > .ant-select-dropdown-menu > :nth-child(3)',
                    DROPDOWN_RECEITA: '.ant-select-dropdown-menu',
                    LIMPAR: '.mr-3',
                    FILTRAR: '.ant-drawer__footer > .ant-btn-primary',
                    TELA_VAZIA: '.ant-tabs-tabpane-active > .ant-empty > .ant-empty-description',

                }

            }
        },

        FISCAL: {
            CODIGO: '.ant-form > .row > :nth-child(1) > .ant-input',
            EAN: '.ant-form > .row > :nth-child(2) > .ant-input',
            NCM: '#Ncm',
            EX: '.ant-form > .row > :nth-child(4) > .form-control',
            DESCRICAO: '.col-md-12 > .ant-input',
            NAO_CLASSIFICADO: '[value="=0"]',
            NAO_CLASSIFICADO_ICMS: ':nth-child(4) > .products__actions > .products__actions___filters > [value="=0"]',
            CLASSIFICADO: '[value="=1"]',
            DUPLICADO: '#sDuplicado',
            DUPLICADO_ICMS: ':nth-child(4) > .products__actions > .products__actions___filters > #sDuplicado',
            CLASSIFICADO_ICMS: '.products__actions___filters > :nth-child(2)',
            EXPORTAR_ITENS_REVISAO: '.products__actions___filters > .ant-btn',
            EXPORTAR_ITENS_REVISAO_SIM: '.ant-popover-buttons > .ant-btn-primary',
            EXPORTAR_ITENS_REVISAO_NAO: '.ant-popover-buttons > :nth-child(1)',
            SAIR_CLASSIFICAR: '.btn-divergence',
            NUM_SELECIONADOS: '.mt-1',
            CLASSIFICAR_MANUALMENTE: '.products__actions > :nth-child(2) > :nth-child(2)',
            CLASSIFICAR_MANUALMENTE_ICMS: ':nth-child(4) > .products__actions > :nth-child(2) > :nth-child(2)',
            CLASSIFICAR_ECONSULTA: '.products__actions > :nth-child(2) > :nth-child(1)',
            CLASSIFICAR_ECONSULTA2: ':nth-child(4) > .products__actions > :nth-child(2) > :nth-child(1)',
            PAGINACAO: ':nth-child(3) > .ant-pagination > .ant-pagination-total-text',
            LIBERTA_BOTAO_TODOS: '.ant-table-fixed-left > .ant-table-header > .ant-table-fixed > .ant-table-thead > tr > .ant-table-selection-column > .ant-table-header-column > :nth-child(1) > .ant-table-column-title > .ant-table-selection > .ant-table-selection-down > .anticon > svg',
            LIBERTA_BOTAO_TODOS2: '.ant-table-fixed-left > .ant-table-header > .ant-table-fixed > .ant-table-thead > tr > .ant-table-selection-column > .ant-table-header-column > :nth-child(1) > .ant-table-column-title > .ant-table-selection > .ant-table-selection-down',
            LIBERTA_BOTAO_TODOS3: '.ant-tabs-tabpane-active > .ant-table-wrapper > .ant-spin-nested-loading > .ant-spin-container > .ant-table > .ant-table-content > .ant-table-fixed-left > .ant-table-header > .ant-table-fixed > .ant-table-thead > tr > .ant-table-selection-column > .ant-table-header-column > :nth-child(1) > .ant-table-column-title > .ant-table-selection > .ant-table-selection-down',
            BOTAO_TODOS: '.ant-dropdown-menu-item',
            BOTAO_TODOS2: ':nth-child(4) > .ant-table-wrapper > .ant-spin-nested-loading > .ant-spin-container > .ant-table > [style="position: absolute; top: 0px; left: 0px; width: 100%;"] > :nth-child(1) > .ant-dropdown > .ant-dropdown-menu > .ant-dropdown-menu-item > div',
            SELECIONAR_TUDO_PAGINA_PIS: '.ant-table-fixed-left > .ant-table-header > .ant-table-fixed > .ant-table-thead > tr > .ant-table-selection-column > .ant-table-header-column > :nth-child(1) > .ant-table-column-title > .ant-table-selection > .ant-table-selection-select-all-custom > .ant-checkbox > .ant-checkbox-input',
            SELECIONAR_TUDO_PAGINA_ICMS: ':nth-child(4) > .ant-table-wrapper > .ant-spin-nested-loading > .ant-spin-container > .ant-table > .ant-table-content > .ant-table-fixed-left > .ant-table-header > .ant-table-fixed > .ant-table-thead > tr > .ant-table-selection-column > .ant-table-header-column > :nth-child(1) > .ant-table-column-title > .ant-table-selection > .ant-table-selection-select-all-custom > .ant-checkbox > .ant-checkbox-input',
        },


    },

    ERECUPERADOR_BODY: {
        VAZIO: '.ant-empty-description',
        TELA_PRINCIPAL: '.ant-layout-content',
        EMPRESAS: '.item__action > .item__action___header',
        CNPJ_EMPRESAS: '.item__action > .item__action___header > .header__cnpj',
        FISCAL_VAZIO: '.ant-tabs-tabpane-active > .ant-empty',
        TABELA_FISCAL: '.ant-table-row',
        HEADER: '.ant-page-header-heading',
        MODAL: '.ant-modal-wrap',
    },

    ERECUPERADOR_ABAS: {
        INFORMACOES: '.ant-tabs-nav > :nth-child(1) > :nth-child(1):eq(0)',
        DOCUMENTOS: '.ant-tabs-nav > :nth-child(1) > :nth-child(2):eq(0)',
        FISCAL: '.ant-tabs-nav > :nth-child(1) > :nth-child(3):eq(0)',
        RESULTADOS: '.ant-tabs-nav > :nth-child(1) > :nth-child(4)',

        SEG_FISCAL_ICMS: '.ant-tabs-nav > :nth-child(1) > :nth-child(4):eq(0)',
        SEG_RESULTADOS: '.ant-tabs-nav > :nth-child(1) > :nth-child(5)',



        INFORMACOES_DADOS: ':nth-child(1) > .ant-tabs > .ant-tabs-bar > .ant-tabs-nav-container > .ant-tabs-nav-wrap > .ant-tabs-nav-scroll > .ant-tabs-nav > :nth-child(1) > .ant-tabs-tab-active',
        INFORMACOES_CFOP: ':nth-child(1) > .ant-tabs > .ant-tabs-bar > .ant-tabs-nav-container > .ant-tabs-nav-wrap > .ant-tabs-nav-scroll > .ant-tabs-nav > :nth-child(1) > :nth-child(2)',
        INFORMACOES_CONFIGURACOES: ':nth-child(1) > .ant-tabs > .ant-tabs-bar > .ant-tabs-nav-container > .ant-tabs-nav-wrap > .ant-tabs-nav-scroll > .ant-tabs-nav > :nth-child(1) > :nth-child(3)',

    },

    ERECUPERADOR_VISUALIZAR: {
        ABA_CFOP: '.ant-tabs-nav-scroll>>>:eq(2)',
        ABA_RECEITAS: '.ant-tabs-nav-scroll>>>:eq(1)',
        ABA_CFOP_2: '.ant-tabs-nav-scroll>>>:eq(1)',

        APURAMENTO: '.ant-tabs-nav-scroll :eq(1) :nth-child(1)',
        DETALHAMENTO: '.ant-tabs-nav-scroll :eq(1) :nth-child(2)',
        CFOP: '.ant-tabs-nav-scroll :eq(1) :nth-child(3)',
        BAIXAR_RELATORIO: '.btn-print',

        DETALHAMENTO: {
            TABELA: '.ant-table-scroll > .ant-table-body',
            BTN_FILTRO: '.d-flex > .ant-btn',

            FILTRO_MODAL: {
                MODAL: '.ant-drawer-body',
                DESCRIÇÃO: '.ant-col-24 > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                CODIGO: ':nth-child(2) > :nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                NCM: ':nth-child(2) > :nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .form-control',
                EX: ':nth-child(3) > :nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                EAN: ':nth-child(3) > :nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                TRIBUTACAO_PIS_DROPDOWN: '.ant-select-dropdown-menu >',
                TRIBUTACAO_PIS_CAMPO: ':nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-select--custom > .ant-select-selection',
                TRIBUTACAO_ICMS_DROPDOWN: '.ant-select-dropdown-menu >',
                TRIBUTACAO_ICMS_CAMPO: ':nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-select--custom > .ant-select-selection',
                N_DOC: ':nth-child(5) > :nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                MODELO: ':nth-child(5) > :nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                CFOP: ':nth-child(6) > :nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                CST_ICMS: ':nth-child(6) > :nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                CHAVE: ':nth-child(7) > :nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                INDICADOR: ':nth-child(7) > :nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                SITUACAO: ':nth-child(8) > :nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .ant-input',
                QUANTIDADE: ':nth-child(8) > :nth-child(2) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .form-control',
                VALOR: ':nth-child(9) > :nth-child(1) > .ant-row > .ant-form-item-control-wrapper > .ant-form-item-control > .ant-form-item-children > .form-control',
                DIA: '.ant-input-wrapper > .ant-input',
                COMPETENCIA: '.ant-input-group-addon',
                CLOSE: '.ant-drawer-close > .anticon > svg',
                BTN_LIMPAR: '.mr-3',
                BTN_FILTRAR: '.ant-btn-primary',
            },
        },




        VISUALIZAR_CFOP: {
            TABELA_CONSIDERA: '.table--success > .ant-spin-nested-loading > .ant-spin-container > .ant-table > .ant-table-content',
            TABELA_DESCONSIDERA: '.table--danger > .ant-spin-nested-loading > .ant-spin-container > .ant-table > .ant-table-content',
            TABELA_VAZIA: '.ant-empty',


        },

        COMPLETO: {
            RECEITA_DECLARADA_VALOR: ':nth-child(1) > .ant-card > .ant-card-body',
            RECEITA_APURADA: ':nth-child(2) > .ant-card > .ant-card-body',
            DIFERENCAS: ':nth-child(3) > .ant-card > .ant-card-body',
            PROSSEGUIR: '.btn-ok',


            MODAL_PROSSEGUIR: {
                BTN_PROSSEGUIR: '.ant-btn-warning',
                BTN_PROSSEGUIR_DIFERENCA: '.ant-btn-danger',
                BTN_CANCELAR: '.ant-modal-footer > :nth-child(1)',

            }

        },

        APURACAO_CREDITO: {
            VALOR_AJUSTADO: '.segregation__info > :nth-child(1)',
            VALOR_TOTAL: ':nth-child(1) > .ant-collapse-header > .ant-collapse-extra > span',

            OUTRAS_RECEITAS: ':nth-child(2) > .ant-collapse-header > .ant-collapse-extra > span',
            OUTRAS_RECEITAS_2: ':nth-child(3) > .ant-collapse-header > .ant-collapse-extra > span',
            ATIVIDADE1: ':nth-child(1) > .ant-collapse-header > .ant-collapse-extra > span',
            ATIVIDADE2: ':nth-child(2) > .ant-collapse-header > .ant-collapse-extra > span',
            TOTAIS_ESTABELECIMENTOS: ':nth-child(3) > .ant-collapse-header > .ant-collapse-extra > span',
            TOTAIS_ESTABELECIMENTOS_2: ':nth-child(2) > .ant-collapse-header > .ant-collapse-extra > span',
            TOTAIS_ESTABELECIMENTOS_3: ':nth-child(4) > .ant-collapse-header > .ant-collapse-extra > span',

            RECEITA_TOTAL_INFORMADA: ':nth-child(4) > .ant-collapse-header > .ant-collapse-extra > span',
            RECEITA_TOTAL_INFORMADA_2: ':nth-child(3) > .ant-collapse-header > .ant-collapse-extra > span',
            RECEITA_TOTAL_INFORMADA_3: ':nth-child(5) > .ant-collapse-header > .ant-collapse-extra > span',
            TOTAL_PIS_COFINS: '.segregation__total___value',
            TOTAL_ICMS: ':nth-child(6) > .segregation__total___value',
            PARCELA1: ':nth-child(1) > .pt-3 > :nth-child(1)',
            PARCELA2: ':nth-child(4) > .pt-3 > :nth-child(1)',
        },


    },

    TERCEIROS: {
        MENU: {
            INICIO: ':nth-child(3) > :nth-child(1) > >:eq(2)',
            CADASTRAR: ':nth-child(2) > >>>>>>>>:eq(5)',
            LISTAR: ':nth-child(4) >>:eq(1)',
            MENU_EMPRESAS: '.MuiDrawer-root-181 > .MuiPaper-root-80',
            III: 'button:eq(0)>:eq(0)'
        },

        EMPRESA: {
            REMOVER_PRIMEIRO: '[title="Apagar empresa"]:eq(0)',
            REMOVER_SEGUNDO: '[title="Apagar empresa"]:eq(1)',
            EDITAR_PRIMEIRO: '[title="Editar empresa"]:eq(0)',
            FILTRO_EMPRESAS: '#outlined-basic',
        },

        ABAS_EMPRESA: {
            CONFIGURACOES: '#simple-tab-0',
            DOCUMENTOS: '#simple-tab-1',
            RESULTADOS: '#simple-tab-2',
        },

        MODAL_CADASTRO: {

            P_RAZAO: 'input:eq(0)',
            P_CNPJ: '#cnpj-input',
            P_SWITCH_COOPERATIVA: 'input:eq(2)',
            P_RUA: 'input:eq(3)',
            P_NUMERO: 'input:eq(4)',
            P_COMPLETO: 'input:eq(5)',
            P_BAIRRO: 'input:eq(6)',
            P_MUNICIPIO: 'input:eq(7)',
            P_DROPDOWN_UF: ':nth-child(9)>#label-uf-cadastro-empresa',
            P_DROPDOWN_UF_2: ' #label-uf-cadastro-empresa',

            CEP: 'input:eq(9)',
            BTN_CANCELAR: 'button:eq(5)',
            BTN_CONFIRMAR: 'button:eq(6)',

            RAZAO: 'input:eq(2)',
            CNPJ: '#cnpj-input',
            SWITCH_COOPERATIVA: 'input:eq(4)',
            RUA: 'input:eq(5)',
            NUMERO: 'input:eq(6)',
            COMPLETO: 'input:eq(7)',
            BAIRRO: 'input:eq(8)',
            MUNICIPIO: 'input:eq(9)',
            CEP: 'input:eq(11)',
            DROPDOWN_UF: ' :nth-child(9)>#label-uf-cadastro-empresa',
            DROPDOWN_UF_2: ' #label-uf-cadastro-empresa',           

            RAZAO_2: 'input:eq(2)',
            CNPJ_2: '#cnpj-input',
            RUA_2: 'input:eq(4)',
            NUMERO_2: 'input:eq(5)',
            COMPLETO_2: 'input:eq(6)',
            BAIRRO_2: 'input:eq(7)',
            MUNICIPIO_2: 'input:eq(8)',
            CEP_2: 'input:eq(10)',
            DROPDOWN_UF_X: ' :nth-child(8)>#label-uf-cadastro-empresa',
            DROPDOWN_UF_X2: ' #label-uf-cadastro-empresa',

            EDIT_RAZAO: 'input:eq(2)',
            EDIT_CNPJ: '#cnpj-input',
            EDIT_RUA: 'input:eq(4)',
            EDIT_NUMERO: 'input:eq(5)',
            EDIT_COMPLETO: 'input:eq(6)',
            EDIT_BAIRRO: 'input:eq(7)',
            EDIT_MUNICIPIO: 'input:eq(8)',
            EDIT_UF: 'input:eq(9)',
            EDIT_CEP: 'input:eq(10)',

        },

        CONFIGURACOES: {
            ALICOTAS: '.MuiTabs-root > .MuiTabs-scroller > .MuiTabs-flexContainer > #simple-tab-0:eq(1)',
            CALCULO: '.MuiTabs-root > .MuiTabs-scroller > .MuiTabs-flexContainer > #simple-tab-1:eq(1)',
        },

        DOCUMENTOS: {
            DRAG_N_DROP: '.MuiBox-root'
        },

        RESULTADOS: {
            PAGINACAO: '.MuiToolbar-root > :nth-child(4)'
        },

        MODAL_APAGAR: {

            APAGAR: 'button:eq(9)',
            CANCELAR: 'button:eq(8)',

        }

    },

    VERBAS: {
        MENU: {
            INICIO: ':nth-child(3) > :nth-child(1) > >:eq(2)',
            CADASTRAR: ':nth-child(2) > >>>>>>>>:eq(5)',
            LISTAR: ':nth-child(4) >>:eq(1)',
            MENU_EMPRESAS: '.MuiDrawer-root-181 > .MuiPaper-root-80',
            III: 'button:eq(0)'
        },

        EMPRESA: {
            REMOVER_PRIMEIRO: '[title="Apagar empresa"]',
            EDITAR_PRIMEIRO: '[style="display: inline-grid;"] > :nth-child(2)',
            FILTRO_EMPRESAS: '#outlined-basic',
        },

        ABAS_EMPRESA: {
            CONFIGURACOES: '#simple-tab-0',
            DOCUMENTOS: '#simple-tab-1',
            SELECAO_RUBRICAS: '#simple-tab-2',
            TRABALHADORES: '#simple-tab-3',
            RUBRICAS: '#simple-tab-4',
            RESULTADOS: '#simple-tab-5',
        },

        MODAL_CADASTRO: {

            RAZAO: 'input:eq(0)',
            CNPJ: '#cnpj-input',

            RUA: 'input:eq(2)',
            NUMERO: 'input:eq(3)',
            COMPLETO: 'input:eq(4)',
            BAIRRO: 'input:eq(5)',
            MUNICIPIO: 'input:eq(6)',
            DROPDOWN_UF: ':nth-child(8)>#label-uf-cadastro-empresa',
            DROPDOWN_UF_2: ' #label-uf-cadastro-empresa',

            CEP: 'input:eq(8)',
            BTN_CANCELAR: 'button:eq(5)',
            BTN_CONFIRMAR: 'button:eq(6)',


            ALT_RAZAO: 'input:eq(1)',
            ALT_CNPJ: 'input:eq(2)',
            ALT_SWITCH_COOPERATIVA: 'input:eq(3)',
            ALT_RUA: 'input:eq(4)',
            ALT_NUMERO: 'input:eq(5)',
            ALT_COMPLETO: 'input:eq(6)',
            ALT_BAIRRO: 'input:eq(7)',
            ALT_MUNICIPIO: 'input:eq(8)',
            ALT_DROPDOWN_UF: 'input:eq(9)',
            ALT_CEP: 'input:eq(10)',
            ALT_BTN_CANCELAR: 'button:eq(8)',
            ALT_BTN_CONFIRMAR: 'button:eq(9)',

        },

        CONFIGURACOES: {
            PERIODOS: '.MuiTabs-root > .MuiTabs-scroller > .MuiTabs-flexContainer > #simple-tab-0:eq(1)',
            DATA: '.MuiTabs-root > .MuiTabs-scroller > .MuiTabs-flexContainer > #simple-tab-1:eq(1)',
        },

        SELECAO: {
            AUXILIO: ':nth-child(1) > :nth-child(6) > .MuiButtonBase-root > .MuiButton-label > .MuiSvgIcon-root',
            AVISO: ':nth-child(2) > :nth-child(6) > .MuiButtonBase-root > .MuiButton-label > .MuiSvgIcon-root',
            MATERNIDADE: ':nth-child(3) > :nth-child(6) > .MuiButtonBase-root > .MuiButton-label > .MuiSvgIcon-root',
            DECIMO: ':nth-child(4) > :nth-child(6) > .MuiButtonBase-root > .MuiButton-label > .MuiSvgIcon-root',
            PAT: ':nth-child(5) > :nth-child(6) > .MuiButtonBase-root > .MuiButton-label > .MuiSvgIcon-root',
            OUTRAS: ':nth-child(6) > :nth-child(6) > .MuiButtonBase-root > .MuiButton-label > .MuiSvgIcon-root',

            SWITCHS: {
                AUXILIO: ' div > .MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label :eq(0)',
                AUXILIO2: '.MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label > :eq(1)',
                MATERNIDADE: ' div > .MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label :eq(4)',
                MATERNIDADE2: '.MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label > :eq(5)',
                OUTROS: '.MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label > :eq(10)',
                OUTROS2: 'div > .MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label :eq(11)',
            },

            MODAL: {
                SWITCH_1: '.MuiGrid-container > :nth-child(2)>',
                X: '.MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root',
                X2: '.MuiToolbar-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root:eq(1)',
                FILTRO: '.MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input'
            }
        },

        DOCUMENTOS: {
            DRAG_N_DROP: '.MuiBox-root'
        },

        RESULTADOS: {
            PAGINACAO: '.MuiToolbar-root > :nth-child(4)'
        },

        MODAL_APAGAR: {

            APAGAR: 'button:eq(9)',
            CANCELAR: 'button:eq(8)',

        }

    },

    ISSQN: {
        MENU: {
            INICIO: ':nth-child(3) > :nth-child(1) > >:eq(2)',
            CADASTRAR: ':nth-child(2) > >>>>>>>>:eq(5)',
            LISTAR: ':nth-child(4) >>:eq(1)',
            MENU_EMPRESAS: '.MuiDrawer-root-181 > .MuiPaper-root-80',
            III: 'button:eq(0)'
        },

        ABAS_EMPRESA: {
            CONFIGURACOES: '#simple-tab-0',
            DOCUMENTOS: '#simple-tab-1',
            RESULTADOS_PIS: '#simple-tab-2',
            RESULTADOS_COFINS: '#simple-tab-3',
        },

        CONFIGURACOES: {
            SWITCHS: {
                BROCOA: ' div > .MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label :eq(0)',
                BROCOA2: '.MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label > :eq(1)',
                BROCOM: ' div > .MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label :eq(2)',
                BROCOM2: '.MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label > :eq(3)',
                PLANILHA: ' div > .MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label :eq(4)',
                PLANILHA2: '.MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label > :eq(5)',
            },

        },

        MODAL: {
            MENSAGEM: '#alert-dialog-description',
            PROSSEGUIR: '.MuiButton-textSecondary',
            CANCELAR: '.MuiDialogActions-root > :nth-child(1)',
            BAIXAR_PLANILHA: '#alert-dialog-description > .MuiButtonBase-root',
        },

        RESULTADOS: {
            PAGINACAO: '.MuiToolbar-root > :nth-child(4)'
        },

    },

    DIFAL: {
        MENU: {
            INICIO: ':nth-child(3) > :nth-child(1) > >:eq(2)',
            CADASTRAR: ':nth-child(2) > >>>>>>>>:eq(5)',
            LISTAR: ':nth-child(4) >>:eq(1)',
            MENU_EMPRESAS: '.MuiDrawer-root-181 > .MuiPaper-root-80',
            III: 'button:eq(0)>:eq(0)'
        },

        EMPRESA: {
            REMOVER_PRIMEIRO: '[title="Apagar empresa"]:eq(0)',
            EDITAR_PRIMEIRO: '[title="Editar empresa"]:eq(0)',
            FILTRO_EMPRESAS: '#outlined-basic',
        },

        ABAS_EMPRESA: {
            
            DOCUMENTOS: '#simple-tab-0',
            RESULTADOS: '#simple-tab-1',
        },

        MODAL_CADASTRO: {

            P_RAZAO: 'input:eq(0)',
            P_CNPJ: '#cnpj-input',
            P_SWITCH_COOPERATIVA: 'input:eq(2)',
            P_RUA: 'input:eq(3)',
            P_NUMERO: 'input:eq(4)',
            P_COMPLETO: 'input:eq(5)',
            P_BAIRRO: 'input:eq(6)',
            P_MUNICIPIO: 'input:eq(7)',
            P_DROPDOWN_UF: ':nth-child(9)>#label-uf-cadastro-empresa',
            P_DROPDOWN_UF_2: ' #label-uf-cadastro-empresa',

            CEP: 'input:eq(9)',
            BTN_CANCELAR: 'button:eq(5)',
            BTN_CONFIRMAR: 'button:eq(6)',

            RAZAO: 'input:eq(2)',
            CNPJ: '#cnpj-input',
            SWITCH_COOPERATIVA: 'input:eq(4)',
            RUA: 'input:eq(5)',
            NUMERO: 'input:eq(6)',
            COMPLETO: 'input:eq(7)',
            BAIRRO: 'input:eq(8)',
            MUNICIPIO: 'input:eq(9)',
            CEP: 'input:eq(11)',
            DROPDOWN_UF: ' :nth-child(9)>#label-uf-cadastro-empresa',
            DROPDOWN_UF_2: ' #label-uf-cadastro-empresa',

            RAZAO_2: 'input:eq(2)',
            CNPJ_2: '#cnpj-input',
            RUA_2: 'input:eq(4)',
            NUMERO_2: 'input:eq(5)',
            COMPLETO_2: 'input:eq(6)',
            BAIRRO_2: 'input:eq(7)',
            MUNICIPIO_2: 'input:eq(8)',
            CEP_2: 'input:eq(10)',
            DROPDOWN_UF_X: ' :nth-child(8)>#label-uf-cadastro-empresa',
            DROPDOWN_UF_X2: ' #label-uf-cadastro-empresa',

        },

        DOCUMENTOS: {
            DRAG_N_DROP: '.MuiBox-root'
        },

        RESULTADOS: {
            PAGINACAO: '.MuiToolbar-root > :nth-child(4)'
        },

        MODAL_APAGAR: {

            APAGAR: 'button:eq(9)',
            CANCELAR: 'button:eq(8)',

        }

    },

    ICMS: {
        MENU: {
            INICIO: ':nth-child(3) > :nth-child(1) > >:eq(2)',
            CADASTRAR: ':nth-child(2) > >>>>>>>>:eq(5)',
            LISTAR: ':nth-child(4) >>:eq(1)',
            MENU_EMPRESAS: '.MuiDrawer-root-181 > .MuiPaper-root-80',
            III: 'button:eq(0)>:eq(0)'
        },

        EMPRESA: {
            REMOVER_PRIMEIRO: '[title="Apagar empresa"]:eq(0)',
            EDITAR_PRIMEIRO: '[title="Editar empresa"]:eq(0)',
            FILTRO_EMPRESAS: '#outlined-basic',
        },

        ABAS_EMPRESA: {
            CONFIGURACOES: '#simple-tab-0',
            DOCUMENTOS: '#simple-tab-1',
            RESULTADOS_PIS: '#simple-tab-2',
            RESULTADOS_COFINS: '#simple-tab-3',
        },

        MODAL_CADASTRO: {

            P_RAZAO: 'input:eq(0)',
            P_CNPJ: '#cnpj-input',
            P_SWITCH_COOPERATIVA: 'input:eq(2)',
            P_RUA: 'input:eq(3)',
            P_NUMERO: 'input:eq(4)',
            P_COMPLETO: 'input:eq(5)',
            P_BAIRRO: 'input:eq(6)',
            P_MUNICIPIO: 'input:eq(7)',
            P_DROPDOWN_UF: ':nth-child(9)>#label-uf-cadastro-empresa',
            P_DROPDOWN_UF_2: ' #label-uf-cadastro-empresa',

            CEP: 'input:eq(9)',
            BTN_CANCELAR: 'button:eq(5)',
            BTN_CONFIRMAR: 'button:eq(6)',

            RAZAO_1: 'input:eq(1)',
            RAZAO: 'input:eq(2)',
            CNPJ: '#cnpj-input',
            SWITCH_COOPERATIVA: 'input:eq(4)',
            RUA: 'input:eq(5)',
            NUMERO: 'input:eq(6)',
            COMPLETO: 'input:eq(7)',
            BAIRRO: 'input:eq(8)',
            MUNICIPIO: 'input:eq(9)',
            CEP: 'input:eq(11)',
            DROPDOWN_UF: ' :nth-child(9)>#label-uf-cadastro-empresa',
            DROPDOWN_UF_2: ' #label-uf-cadastro-empresa',

            RAZAO_2: 'input:eq(2)',
            CNPJ_2: '#cnpj-input',
            RUA_2: 'input:eq(4)',
            NUMERO_2: 'input:eq(5)',
            COMPLETO_2: 'input:eq(6)',
            BAIRRO_2: 'input:eq(7)',
            MUNICIPIO_2: 'input:eq(8)',
            CEP_2: 'input:eq(10)',
            DROPDOWN_UF_X: ' :nth-child(8)>#label-uf-cadastro-empresa',
            DROPDOWN_UF_X2: ' #label-uf-cadastro-empresa',

        },

        DOCUMENTOS: {
            DRAG_N_DROP: '.MuiBox-root'
        },

        RESULTADOS: {
            PAGINACAO: '.MuiToolbar-root > :nth-child(4)'
        },

        CONFIGURACOES:{
            INPUT_BUSCA:'.MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input',
            SWITCH_1:'[style="min-width: 150px;"] > .MuiGrid-container > :nth-child(2) > .MuiSwitch-root > .MuiButtonBase-root > .MuiIconButton-label',
        },

        MODAL_APAGAR: {

            APAGAR: 'button:eq(9)',
            CANCELAR: 'button:eq(8)',

        }

    },

    MESSAGE: {
        POPUP: '.ant-message-notice-content',
        TOASTIFY: '.ant-notification-notice-message',
        CLOSE: '.ant-notification-close-x',
        MSG: '.ant-notification-notice-description',
        POPUP_TERCEIROS: '#notistack-snackbar',
        POPUP_VERBAS: '#client-snackbar',
        POPUP_ISSQN: '.MuiSnackbarContent-message > #client-snackbar',
        POPUP_DIFAL: '#notistack-snackbar',
        POPUP_ICMS:'#client-snackbar',

        POPUP_TERCEIROS_2: '.MuiAlert-message',
        POPUP_DIFAL_2: '.MuiAlert-message',
        POPUP_VERBAS_2: '.MuiAlert-message',


        LABEL_ERROR: {
            CADASTRO_RAZAO: ':nth-child(1) > .col-form-label-error',
            CADASTRO_CNPJ: ':nth-child(2) > .col-form-label-error',
            CADASTRO_UF: ':nth-child(4) > .col-form-label-error',
        }
    },

    ICONS: {
        OK: '[d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z"]',
        X: '[d="M512 65C264.6 65 64 265.6 64 513s200.6 448 448 448 448-200.6 448-448S759.4 65 512 65zm0 820c-205.4 0-372-166.6-372-372s166.6-372 372-372 372 166.6 372 372-166.6 372-372 372z"]',

        BUTTONS: {
            CLASSIFICACAO: '.ant-table-body-inner > .ant-table-fixed > .ant-table-tbody > .ant-table-row > .ant-table-row-cell-break-word > .ant-btn-group > .btn-warning',
            REPROCESSAR: '.ant-table-body-inner > .ant-table-fixed > .ant-table-tbody > .ant-table-row > .ant-table-row-cell-break-word > .ant-btn-group > :nth-child(2)',
            APAGAR: '.ant-table-body-inner > .ant-table-fixed > .ant-table-tbody > .ant-table-row > .ant-table-row-cell-break-word > .ant-btn-group > .ant-btn-danger',
            VIZUALIZAR: '.ant-table-body-inner > .ant-table-fixed > .ant-table-tbody > .ant-table-row > .ant-table-row-cell-break-word > .ant-btn-group > span',
            VIZUALIZAR_DISABLE: '.ant-btn-group > span',
        }
    }

}





export default locators;