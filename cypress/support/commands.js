import 'cypress-file-upload';
import loc from '../support/locators'

import { baseLogada, senhaPadrao } from '../support/base.js'


//var link = UrlTestada
var link = Cypress.env("url")
let urlQA = 'https://contateste.e-auditoria.com.br/Login'
let urlProd = 'https://conta.e-auditoria.com.br/Login'

// FUNÇÕES DE LOGIN

Cypress.Commands.add('login', (op) => {

    cy.clearCookies()
    cy.clearLocalStorage()
    cy.visit(link, { timeout: 10000, force: true })

    cy.get(loc.LOGIN.USER, { timeout: 30000 }).should('be.visible').type(baseLogada)

    cy.get(loc.LOGIN.PASSWORD).type(senhaPadrao)
    cy.get(loc.LOGIN.BTN_LOGIN, { timeout: 50000 }).click({ force: true })

    cy.get('.title', { timeout: 30000 }).should('be.visible').should('contain', 'Todas as ferramentas que você precisa em um único lugar')



    if (op == 1) {
        cy.acessoRecupecaoPIS()
    }

    else if (op == 2) {
        cy.acessoSegregacao()
    }

    else if (op == 3) {
        cy.acessoTerceiros()
    }

    else if (op == 4) {
        cy.acessoVerbas()
    }

    else if (op == 5) {
        cy.acessoISSQN()
    }

    else if (op == 6) {
        cy.acessoDIFAL()
    }

    else if (op == 7) {
        cy.acessoEXC_ICMS()
    }

    // Cypress.on('uncaught:exception', (err, runnable) => {
    //     return false
    // })

})

Cypress.Commands.add('waitLoading', () => {
    cy.get('.loading', { timeout: 50000 }).should('not.exist')
})

Cypress.Commands.add('oldLogoff', (op) => {
    cy.get(':nth-child(10) > .fas', { timeout: 5000 }).should('be.visible')
    cy.get(':nth-child(10) > .fas').click({ timeout: 5000 })
    cy.get(loc.MENU_ERECUPERADOR.SAIR_CONTA).click({ timeout: 5000 })
    cy.wait(3000)
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.visit(link, { timeout: 10000, force: true })
    cy.get('.titulo-form', { timeout: 20000 }).should('contain', 'Seja bem vindo!')
    cy.url({ timeout: 30000 }).should('eq', link)
    cy.login(op)

})

Cypress.Commands.add('logoff', (op) => {

    cy.clearCookies()
    cy.clearLocalStorage()
    cy.visit(link, { timeout: 10000, force: true })
    cy.reload()
    cy.get('.titulo-form', { timeout: 20000 }).should('be.visible')
    cy.get('.titulo-form', { timeout: 20000 }).should('contain', 'Seja bem vindo!')
    cy.url({ timeout: 30000 }).should('eq', link)
    cy.login(op)

})

//FUNÇÕES DE ACESSO DE MODULOS

Cypress.Commands.add('acessoRecupecaoPIS', () => {
    cy.get(loc.MODULES.ERECUPERADOR, { timeout: 30000 }).should('be.visible').click({ force: true })
    cy.get('.block_tela', { timeout: 50000 }).should('not.exist')

    cy.waitLoading()

    cy.crashContrato()

    cy.get(loc.MENU_ERECUPERADOR.SIMPLES_NACIONAL, { timeout: 300000 }).should('exist').should('contain', 'acesse aqui').click({ force: true })

    cy.get(loc.MENU_ERECUPERADOR_SIMPLES_NACIONAL.RECUPERACAO_PIS, { timeout: 300000 }).should('be.visible').click({ force: true })

})

Cypress.Commands.add('acessoSegregacao', () => {
    cy.get(loc.MODULES.ERECUPERADOR, { timeout: 30000 }).should('be.visible').click({ force: true })
    cy.get('.block_tela', { timeout: 50000 }).should('not.exist')

    cy.waitLoading()

    cy.crashContrato()

    cy.get(loc.MENU_ERECUPERADOR.SIMPLES_NACIONAL, { timeout: 300000 }).should('exist').should('contain', 'acesse aqui').click({ force: true })

    cy.get(loc.MENU_ERECUPERADOR_SIMPLES_NACIONAL.SEGRECACAO_RECEITAS, { timeout: 300000 }).should('be.visible').click({ force: true })

})

Cypress.Commands.add('acessoTerceiros', () => {

    if (urlQA == link) {
        cy.visit('https://contribuicoesterceirosteste.e-auditoria.com.br/home')
        cy.waitLoading()
    }

    else if (urlProd == link) {
        cy.visit('https://contribuicoesterceiros.e-auditoria.com.br/home')
        cy.waitLoading()
    }

})

Cypress.Commands.add('acessoVerbas', () => {

    if (urlQA == link) {
        cy.visit('https://verbasindenizatoriasteste.e-auditoria.com.br/home')
        cy.waitLoading()
    }

    else if (urlProd == link) {
        cy.visit('https://verbasindenizatorias.e-auditoria.com.br/home')
        cy.waitLoading()
    }

})

Cypress.Commands.add('acessoISSQN', () => {


    cy.visit('https://exclusaoissteste.e-auditoria.com.br/home')
    cy.waitLoading()
    cy.wait(2000)
    cy.url().then(URL => {

        var texto = URL

        if (texto == 'https://conta.e-auditoria.com.br/Home/Conta') {
            cy.waitLoading()
            cy.visit('https://exclusaoissteste.e-auditoria.com.br/home')
        }


    })
})

Cypress.Commands.add('acessoDIFAL', () => {
    if (urlQA == link) {
        cy.visit('https://difalsimplesteste.e-auditoria.com.br/home')
        cy.waitLoading()
    }

    else if (urlProd == link) {
        cy.visit('https://difalsimples.e-auditoria.com.br/home')
        cy.waitLoading()
    }
})

Cypress.Commands.add('acessoEXC_ICMS', () => {

    if (urlQA == link) {
        cy.visit('https://novoexclusaoicmsteste.e-auditoria.com.br/')
        cy.waitLoading()
    }

    else if (urlProd == link) {
        cy.visit('https://exclusaoicms.e-auditoria.com.br/')
        cy.waitLoading()
    }

})

//FUNÇÕES DE AUTOMATIZAÇÃO PADRONIZADA

Cypress.Commands.add('criarEmpresa', (nomeEmp, cnpjEmp, ufEmp, ieEmp) => {
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.NOVO, { timeout: 5000 }).should('be.visible')
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.NOVO, { timeout: 8000 }).click({ force: true })
    cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME, { timeout: 8000 }).should('be.visible')
    cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type(nomeEmp)
    cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type(cnpjEmp)


    if (ufEmp != null) {
        cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.BOX_UF).click()
        cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.DROPDOWN_UF).contains(ufEmp).wait(500).click()
    }

    if (ieEmp != null) {

    }

    cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).click({ timeout: 5000 })
    cy.get(loc.MESSAGE.POPUP, { timeout: 30000 }).should('contain', 'Empresa cadastrada com sucesso')
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS).should('be.visible').click()


})

Cypress.Commands.add('delete', () => {
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS, { timeout: 50000 }).click({ force: true })
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.PRIMEIRO_REGISTRO, { timeout: 10000 }).should('be.visible').click({ force: true })
    cy.get('.loading', { timeout: 50000 }).should('not.exist')
    cy.get(loc.ERECUPERADOR_ABAS.INFORMACOES, { timeout: 10000 }).should('be.visible').click({ force: true })
    cy.get('.loading', { timeout: 50000 }).should('not.exist')

    cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.APAGAR_EMPRESA, { timeout: 10000 }).should('be.visible')
    cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.APAGAR_EMPRESA, { timeout: 10000 }).click({ force: true })
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.CONFIRMAR_APAGAR, { timeout: 10000 }).should('be.visible').click({ force: true })


})

Cypress.Commands.add('deleteAll', () => {

    cy.get(loc.ERECUPERADOR_BODY.EMPRESAS).then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(emp.length)
        let length = emp.length

        for (let count = 0; count < length; count++) {
            cy.delete()
            cy.get('.ant-modal-wrap', { timeout: 360000 }).should('not.exist')

        }

    })
})

Cypress.Commands.add('PrimeiroRegistro', () => {

    cy.get(loc.ERECUPERADOR_BODY.TELA_PRINCIPAL).should('be.visible').then(btn => {

        var Default = btn.text()
        console.log(Default)

        cy.get(loc.ERECUPERADOR_BODY.TELA_PRINCIPAL).wait(1000).then(btn2 => {

            var Default2 = btn2.text()

            if (Default == Default2) {

                cy.get(loc.ERECUPERADOR_BOTOES.PRIMEIRO_CADASTRAR).click({ timeout: 10000 })

                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type('EMPRESA PADRAO')
                cy.wait(1000)
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type('25.122.123/0001-26')
                cy.wait(1000)
                cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).click({ force: true })
                cy.get(loc.MESSAGE.POPUP, { timeout: 30000 }).should('contain', 'Empresa cadastrada com sucesso')
            }

            else {
                cy.deleteAll()
                cy.get(loc.ERECUPERADOR_BOTOES.PRIMEIRO_CADASTRAR).click({ timeout: 10000 })
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ, { timeout: 10000 }).should('be.visible').type('25.122.123/0001-26')
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME, { timeout: 10000 }).type('EMPRESA PADRAO')
                cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR, { timeout: 30000 }).click({ force: true })
                cy.get(loc.MESSAGE.POPUP, { timeout: 30000 }).should('contain', 'Empresa cadastrada com sucesso')
                ///ERRO com 1 registro só
            }
        })
    })
})

Cypress.Commands.add('PrimeiroRegistroSeg', () => {

    cy.get(loc.ERECUPERADOR_BODY.TELA_PRINCIPAL).should('be.visible').then(btn => {

        var Default = btn.text()
        console.log(Default)

        cy.get(loc.ERECUPERADOR_BODY.TELA_PRINCIPAL).wait(1000).then(btn2 => {

            var Default2 = btn2.text()

            if (Default == Default2) {

                cy.get(loc.ERECUPERADOR_BOTOES.PRIMEIRO_CADASTRAR).click({ timeout: 10000 })

                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type('EMPRESA PADRAO SEGREGACAO')
                cy.wait(1000)
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type('87.602.788/0001-94')
                cy.wait(1000)
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.BOX_UF).click()
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.DROPDOWN_UF).contains('Minas Gerais').wait(500).click()
                cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).click({ force: true })
                cy.get(loc.MESSAGE.POPUP, { timeout: 30000 }).should('contain', 'Empresa cadastrada com sucesso')
            }

            else {

                cy.deleteAll()
                cy.get(loc.ERECUPERADOR_BOTOES.PRIMEIRO_CADASTRAR).click({ timeout: 10000 })

                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type('EMPRESA PADRAO SEGREGACAO')
                cy.wait(1000)
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type('87.602.788/0001-94')
                cy.wait(1000)
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.BOX_UF).click()
                cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.DROPDOWN_UF).contains('Minas Gerais').wait(500).click()
                cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).click({ force: true })
                cy.get(loc.MESSAGE.POPUP, { timeout: 30000 }).should('contain', 'Empresa cadastrada com sucesso')
            }
        })
    })
})

Cypress.Commands.add('classificarEconsulta', () => {
    cy.get(loc.ERECUPERADOR_ABAS.RESULTADOS).click({ timeout: 5000 })
    cy.waitLoading()
    cy.get(loc.ERECUPERADOR_RESULTADO.BTN_CALSSIFICAR).click()
    cy.get(loc.ERECUPERADOR_RESULTADO.MODAL_CLASSIFICAR.CORRIGIR).click()
    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.LIBERTA_BOTAO_TODOS2).click()
    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.BOTAO_TODOS).click()
    cy.get(loc.ERECUPERADOR_BOTOES.FISCAL.CLASSIFICACAO.ECONSULTA).click()
    cy.get(loc.ERECUPERADOR_BOTOES.FISCAL.CLASSIFICACAO.ECONSULTA_OP.SIM).click()

})

Cypress.Commands.add('classificarEconsultaSeg', () => {

    cy.get(loc.ERECUPERADOR_ABAS.FISCAL).click({ timeout: 5000 })
    cy.waitLoading()

    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.LIBERTA_BOTAO_TODOS).click()
    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.BOTAO_TODOS).click()

    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.CLASSIFICAR_ECONSULTA).click()
    cy.get(loc.ERECUPERADOR_BOTOES.FISCAL.CLASSIFICACAO.ECONSULTA_OP.SIM).click()

    cy.get(loc.ERECUPERADOR_ABAS.SEG_FISCAL_ICMS).click({ timeout: 5000 })
    cy.waitLoading()

    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.NAO_CLASSIFICADO_ICMS).click()
    cy.waitLoading()
    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.LIBERTA_BOTAO_TODOS3).click()
    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.BOTAO_TODOS2).click()

    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.CLASSIFICAR_ECONSULTA2).click()

    cy.get(loc.ERECUPERADOR_BOTOES.FISCAL.CLASSIFICACAO.ECONSULTA_OP.SIM).click()
    cy.waitLoading()
    cy.get(loc.MESSAGE.POPUP).should('contain', 'Produto(s) classificado(s) com sucesso')
})

Cypress.Commands.add('classificarEconsultaSegICMS', () => {

    cy.get(loc.ERECUPERADOR_ABAS.SEG_FISCAL_ICMS).click({ timeout: 5000 })
    cy.waitLoading()

    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.NAO_CLASSIFICADO_ICMS).click()
    cy.waitLoading()
    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.LIBERTA_BOTAO_TODOS3).click()
    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.BOTAO_TODOS2).click()

    cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.CLASSIFICAR_ECONSULTA2).click()

    cy.get(loc.ERECUPERADOR_BOTOES.FISCAL.CLASSIFICACAO.ECONSULTA_OP.SIM).click()

    cy.get(loc.MESSAGE.POPUP).should('contain', 'Produto(s) classificado(s) com sucesso')
})

Cypress.Commands.add('removerEmpresa', (cnpj) => {

    cy.get(loc.ERECUPERADOR_BODY.EMPRESAS).then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        let tamanho = emp.length
        console.log(emp)
        console.log(emp.length)
        console.log(emp[tamanho - 1])

        for (let count = 1; count < tamanho; count++) {
            let ex = emp[count].includes(cnpj)
            console.log(ex)
            if (ex) {
                cy.get('.item__action > .item__action___header:eq(' + count + ')').click()
                cy.get(loc.ERECUPERADOR_ABAS.INFORMACOES, { timeout: 10000 }).should('be.visible').click({ force: true })
                cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.APAGAR_EMPRESA, { timeout: 10000 }).should('be.visible').click({ force: true })
                cy.wait(500)
                cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.CONFIRMAR_APAGAR, { timeout: 10000 }).should('be.visible').click({ force: true })
            }
        }

    })

})

Cypress.Commands.add('removerEmpresaCnpj', (cnpj) => {
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_BODY.CNPJ_EMPRESAS).should('be.visible').then(cnpjs => {
        var botoes = cnpjs.text()
        var cnpjEmp = botoes.includes(cnpj)

        if (cnpjEmp) {
            cy.get(loc.ERECUPERADOR_BODY.CNPJ_EMPRESAS).contains(cnpj).click({ force: true })
            cy.get(loc.ERECUPERADOR_ABAS.INFORMACOES, { timeout: 10000 }).should('be.visible').click({ force: true })
            cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.APAGAR_EMPRESA, { timeout: 10000 }).should('be.visible').click({ force: true })
            cy.wait(500)
            cy.get(loc.ERECUPERADOR_BOTOES.INFORMACOES.DADOS.CONFIRMAR_APAGAR, { timeout: 10000 }).should('be.visible').click({ force: true })
        }
    })
})

Cypress.Commands.add('prosseguir', (resuladoTipo) => {

    cy.get(loc.ERECUPERADOR_RESULTADO.BTN_CALSSIFICAR_SEG_2).click()

    if (resuladoTipo == 'PIS') {
        cy.get(loc.ERECUPERADOR_RESULTADO.SEG_PIS).click()
        cy.get(loc.ERECUPERADOR_RESULTADO.MODAL_CLASSIFICAR.PROSSEGUIR).click()
        cy.waitLoading()
    }
    else if (resuladoTipo == 'ICMS') {
        cy.get(loc.ERECUPERADOR_RESULTADO.SEG_PIS).click()
        cy.get(loc.ERECUPERADOR_RESULTADO.MODAL_CLASSIFICAR.PROSSEGUIR).click()
        cy.waitLoading()
    }

    else {
        cy.get(loc.ERECUPERADOR_RESULTADO.MODAL_CLASSIFICAR.PROSSEGUIR).click()
        cy.waitLoading()
    }
})

Cypress.Commands.add('prosseguirMult', (resuladoTipo, bt) => {

    cy.get(loc.ERECUPERADOR_RESULTADO.BTN_CALSSIFICAR_SEG_2 + ':eq(' + bt + ')').click()

    if (resuladoTipo == 'PIS') {
        cy.get(loc.ERECUPERADOR_RESULTADO.SEG_PIS).click()
        cy.get(loc.ERECUPERADOR_RESULTADO.MODAL_CLASSIFICAR.PROSSEGUIR + ':eq(' + bt + ')').click()
        cy.waitLoading()
    }
    else if (resuladoTipo == 'ICMS') {
        cy.get(loc.ERECUPERADOR_RESULTADO.SEG_PIS).click()
        cy.get(loc.ERECUPERADOR_RESULTADO.MODAL_CLASSIFICAR.PROSSEGUIR + ':eq(' + bt + ')').click()
        cy.waitLoading()
    }

    else {
        cy.get(loc.ERECUPERADOR_RESULTADO.MODAL_CLASSIFICAR.PROSSEGUIR + ':eq(' + bt + ')').click()
        cy.waitLoading()
    }
})

//FUNÇÕES DE REMOÇÃO DE IMPEDIMENTOS

Cypress.Commands.add('crashCancel', () => {
    cy.wait(1000)
    cy.get('.ant-btn >').should('be.visible').then(cancel => {
        var botoes = cancel.text()
        var btnCancelar = botoes.includes('Cancelar')

        if (btnCancelar) {
            cy.get('.ant-btn').contains('Cancelar').click({ force: true })
        }
    })
})

Cypress.Commands.add('crashNotification', () => {

    cy.get(loc.ERECUPERADOR_ARQUIVO.SINO.CLOSE).click({ force: true })

})

Cypress.Commands.add('crashFiltro', () => {
    cy.wait(1000)
    cy.get('.ant-btn >').should('be.visible').then(close => {
        var botoes = close.text()
        var btnFechar = botoes.includes('Limpar')

        if (btnFechar) {
            cy.get('.ant-btn').contains('Limpar').click({ force: true })
            cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CLOSE).click({ force: true })
        }
    })
})

Cypress.Commands.add('crashLogoff', (op) => {
    cy.wait(500)
    cy.url().then(URL => {

        var texto = URL

        if (texto == link) {
            cy.waitLoading()
            cy.login(op)
        }


    })
})

Cypress.Commands.add('crashContrato', (op) => {

    cy.wait(1000)
    cy.get('.ant-btn >').should('be.visible').then(contrato => {
        var botoes = contrato.text()
        var ccontrato = botoes.includes('AceitoNão aceito')

        console.log(botoes)

        if (ccontrato) {
            cy.get('.ant-btn >').contains('Não aceito')
            cy.get('.ant-checkbox-input').check()
            cy.get('.ant-btn-primary').click()

            if (op == 1) {
                cy.acessoRecupecaoPIS()
                cy.waitLoading()
                cy.crashLogoff(2)
            }

            else if (op == 2) {
                cy.acessoSegregacao()
                cy.waitLoading()
                cy.crashLogoff(2)
            }

        }
    })

})

Cypress.Commands.add('crashSegregador', () => {
    //ERRO DE RQUISIÇÃO
    cy.wait(2000)
    cy.logoff(2)
    cy.wait(1000)
    cy.waitLoading()
    cy.reload()
    cy.crashLogoff(2)
})

Cypress.Commands.add('selecaoTudoManual', (cla, tipo) => {
    if (cla == 'PIS') {
        cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.NAO_CLASSIFICADO).click()
        cy.waitLoading()
        cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.SELECIONAR_TUDO_PAGINA_PIS).check()
        cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.CLASSIFICAR_MANUALMENTE).click()
    }
    else if (cla == 'ICMS') {

        cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.NAO_CLASSIFICADO_ICMS).click()
        cy.waitLoading()
        cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.SELECIONAR_TUDO_PAGINA_ICMS).check()
        cy.get(loc.ERECUPERADOR_CAMPOS.FISCAL.CLASSIFICAR_MANUALMENTE_ICMS).click()
    }

    cy.get(loc.ERECUPERADOR_BOTOES.FISCAL.CLASSIFICACAO.TIPO).click({ timeout: 5000 })
    cy.get(loc.ERECUPERADOR_BOTOES.FISCAL.FILTRO.DROP, { timeout: 5000 }).contains(tipo).click({ force: true })
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_BOTOES.FISCAL.CLASSIFICACAO.CLASSIFICAR).click({ timeout: 5000 })
    cy.get(loc.MESSAGE.POPUP, { timeout: 5000 }).should('contain', 'Produto(s) classificado(s) com sucesso')
})

//FUNÇÕES DE UPLOAD

Cypress.Commands.add('uploadMax', (ArquivoUpload) => {
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP, { timeout: 100000 }).should('be.visible')
    var caminhoV = 'MaxArquivos/'
    let ArquivoImportado = caminhoV + ArquivoUpload
    cy.uploadGrupo(ArquivoImportado, ArquivoUpload)

})

Cypress.Commands.add('uploadGrupo', (ArquivoImportado, ArquivoUpload) => {
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP, { timeout: 100000 }).should('be.visible')
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP).attachFile(ArquivoImportado, { subjectType: 'drag-n-drop' });
    cy.get(loc.ERECUPERADOR_ARQUIVO.NOME_ARQUIVO, { timeout: 500 }).should('contain', ArquivoUpload)
})

Cypress.Commands.add('uploadCriptografado', (ArquivoImportado, ArquivoUpload) => {
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP, { timeout: 100000 }).should('be.visible')
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP).attachFile(ArquivoImportado, { subjectType: 'drag-n-drop' });
    cy.get(loc.ERECUPERADOR_ARQUIVO.NOME_ARQUIVO, { timeout: 500 }).should('contain', ArquivoUpload)
    cy.contains('Enviar').click({ timeout: 5000 })

    cy.get(loc.ERECUPERADOR_ARQUIVO.STATUS_ATUAL, { timeout: 40000 }).should('contain', 'Finalizado')

})

Cypress.Commands.add('uploadVazio', (ArquivoImportado, ArquivoUpload) => {
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP, { timeout: 100000 }).should('be.visible')
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP).attachFile(ArquivoImportado, { allowEmpty: true, subjectType: 'drag-n-drop' });
    cy.get(loc.ERECUPERADOR_ARQUIVO.NOME_ARQUIVO, { timeout: 500 }).should('contain', ArquivoUpload)
    cy.contains('Enviar').click({ timeout: 5000 })

    cy.get(loc.ERECUPERADOR_ARQUIVO.STATUS_ATUAL, { timeout: 40000 }).should('contain', 'Finalizado')

})

Cypress.Commands.add("uploadMult", (selector, fileUrlArray, type = "") => {
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP, { timeout: 100000 }).should('be.visible')
    const files = [];
    fileUrlArray.forEach((fileUrl) => {
        cy.fixture(fileUrl, "base64").then(Cypress.Blob.base64StringToBlob).then((blob) => {
            const arquivoNome = fileUrl.split("/");
            const name = arquivoNome[arquivoNome.length - 1];
            files.push(new File([blob], name, { type }));
        });
    });
    const event = { dataTransfer: { files: files } };
    return cy.get(selector).trigger("drop", event);
});

Cypress.Commands.add("uploadLoopXML", (init, fin, caminhoComp, ArquivoXML, ext) => {
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP, { timeout: 100000 }).should('be.visible')
    for (var varXml = init; varXml <= fin; varXml++) {
        let ArquivoXML_IMP = caminhoComp + ArquivoXML + varXml + ext
        cy.uploadMult(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP, [ArquivoXML_IMP]);
    }

    cy.uploadVer2(ArquivoXML, 360000)

});

Cypress.Commands.add("uploadVer2", (ArquivoUpload, time) => {
    cy.waitLoading()
    cy.get(loc.ERECUPERADOR_ARQUIVO.DRAG_N_DROP, { timeout: 100000 }).should('be.visible')
    cy.get(loc.ERECUPERADOR_ARQUIVO.NOME_ARQUIVO, { timeout: 5000 }).should('contain', ArquivoUpload)
    cy.contains('Enviar', { timeout: 50000 }).click({ force: true })
    cy.get(loc.ERECUPERADOR_ARQUIVO.STATUS_ATUAL, { timeout: time }).should('contain', 'Finalizado')

});

Cypress.Commands.add("permitir5anosUpload", () => {

    cy.get(loc.ERECUPERADOR_ABAS.INFORMACOES_CONFIGURACOES).click({ timeout: 5000 })

    cy.waitLoading()

    cy.get(':nth-child(2) > .ant-switch').should('be.visible').then(switch2 => {
        var anos5 = switch2.text()
        var booleanSwitch = anos5.includes('Não')

        if (booleanSwitch) {
            cy.get(':nth-child(2) > .ant-switch', { timeout: 40000 }).click({ timeout: 5000 })
            cy.get(loc.MESSAGE.TOASTIFY, { timeout: 30000 }).should('contain', 'Configuração alterada com sucesso')
            cy.get(loc.MESSAGE.CLOSE).click({ timeout: 5000 })
        }
    })

});

Cypress.Commands.add("permitir5anosUploadNotficacao", () => {

    cy.get(loc.ERECUPERADOR_ARQUIVO.SINO.NOTIFICAÇÃO_1).click()
    cy.get(loc.ERECUPERADOR_ARQUIVO.SINO.ABAS).contains('Com ações').click()
    cy.get(loc.ERECUPERADOR_ARQUIVO.SINO.CONFIGURAR).click()
    cy.get(loc.ERECUPERADOR_ARQUIVO.SINO.MODAL_CONFIG.SIM).click()
    cy.get(loc.MESSAGE.MSG, { timeout: 50000 }).should('contain', 'Essa alteração começará a ser válida a partir da próxima importação.')

});

//FUNÇÕES DE CHECAGEM

Cypress.Commands.add('checkExtencao', (arquivo) => {
    let inv = arquivo.split('').reverse().join('')
    let ext = inv.substr(0, 4).split('').reverse().join('')
    console.log(ext);

    if (ext != '.zip' || ext != '.xml' || ext != '.txt' || ext != '.pdf' || ext != 'xlsx' || ext != '.rar') {
        console.log('ext não permitida');
    }

    //ext=='.xls'
})

Cypress.Commands.add('checkDownload', (arquivoDownload) => {
    cy.readFile('cypress/downloads/' + arquivoDownload)
})

Cypress.Commands.add('dataNameCheck', (cnpj, TIPO) => {
    //TIPO -> EXCEL ou PDF 

    var data = new Date()

    var dia = data.getDate().toString()
    var DD = (dia.length == 1) ? '0' + dia : dia
    var mes = (data.getMonth() + 1).toString()
    var MM = (mes.length == 1) ? '0' + mes : mes
    var YYYY = data.getFullYear();
    var hora = data.getHours();
    var HH = (hora.length == 1) ? '0' + hora : hora
    var men = data.getMinutes().toString();
    var MIN = (men.length == 1) ? '0' + men : men

    var segundos = data.getSeconds()

    var seg = segundos.toString();
    var SS = (seg.length == 1) ? '0' + seg : seg

    let seg1 = (segundos + 1).toString();
    var SS1 = (seg1.length == 1) ? '0' + seg1 : seg1

    var seg0 = (segundos - 1).toString();
    var SS0 = (seg0.length == 1) ? '0' + seg0 : seg0

    let nomeArquivo = (cnpj + "-" + DD + "-" + MM + "-" + YYYY + " " + HH + "_" + MIN + "_" + SS + "-" + TIPO + ".zip")
    let nomeArquivo0 = (cnpj + "-" + DD + "-" + MM + "-" + YYYY + " " + HH + "_" + MIN + "_" + SS1 + "-" + TIPO + ".zip")
    let nomeArquivo1 = (cnpj + "-" + DD + "-" + MM + "-" + YYYY + " " + HH + "_" + MIN + "_" + SS0 + "-" + TIPO + ".zip")

    console.log('cypress/downloads/' + nomeArquivo0)
    console.log('cypress/downloads/' + nomeArquivo)
    console.log('cypress/downloads/' + nomeArquivo1)

    cy.readFile('cypress/downloads/' + nomeArquivo || 'cypress/downloads/' + nomeArquivo0 || 'cypress/downloads/' + nomeArquivo1)


})

// FUNÇÕES DE SORT

Cypress.Commands.add('SortRecuperacoes', (SortType) => {

    var SortButton = (SortType - 1) * 9
    cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })
    cy.wait(1000)

    cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {

        let SortC = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(SortC)
        cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })
        cy.wait(500)

        cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {
            let SortD = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(SortD)

            let compare = JSON.stringify(SortD) <= JSON.stringify(SortC)
            console.log(compare)

            if (compare) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
            }

            else {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
            }

            cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })

        })

    })
})

Cypress.Commands.add('SortVisualizacao', (SortType) => {

    var SortButton = (SortType - 1) * 9
    cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })
    cy.wait(1000)

    cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {

        let SortC = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(SortC)
        cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })
        cy.wait(500)

        cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {
            let SortD = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(SortD)

            let compare = JSON.stringify(SortD) >= JSON.stringify(SortC)
            console.log(compare)

            if (compare) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
            }

            else {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
            }

            cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })

        })

    })
})

Cypress.Commands.add('SortResultado', (SortType) => {

    var SortButton = (SortType - 1) * 9
    cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })
    cy.wait(1000)

    cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {

        let SortC = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(SortC)
        cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })
        cy.wait(500)

        cy.get('[data-row-key] > :nth-child(' + SortType + ')').then(items => {
            let SortD = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(SortD)

            let compare = JSON.stringify(SortD) >= JSON.stringify(SortC)
            console.log(compare)

            if (compare) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
            }

            else {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
            }

            cy.get('.ant-table-header-column > .ant-table-column-sorters :eq(' + SortButton + ')').click({ timeout: 5000 })

        })

    })
})

Cypress.Commands.add('SortCFOP', (SortCFOPType) => {

    cy.get(':nth-child(' + SortCFOPType + ') > .ant-table-header-column > .ant-table-column-sorters > .ant-table-column-title').click({ timeout: 5000 })
    cy.wait(1000)

    cy.get('[data-row-key] > :nth-child(' + SortCFOPType + ')').then(items => {

        let SortC = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(SortC)
        cy.get(':nth-child(' + SortCFOPType + ') > .ant-table-header-column > .ant-table-column-sorters > .ant-table-column-title').click({ timeout: 5000 })
        cy.wait(500)

        cy.get('[data-row-key] > :nth-child(' + SortCFOPType + ')').then(items => {
            let SortD = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(SortD)

            let compare = JSON.stringify(SortD) >= JSON.stringify(SortC)
            console.log(compare)

            if (compare) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
            }

            else {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
            }

            cy.get(':nth-child(' + SortCFOPType + ') > .ant-table-header-column > .ant-table-column-sorters > .ant-table-column-title').click({ timeout: 5000 })

        })

    })
})

Cypress.Commands.add('SortClassificacoes', (SortTypeClas) => {
    cy.get(':nth-child(' + SortTypeClas + ') > .ant-table-header-column > .ant-table-column-sorters > .ant-table-column-title').click({ timeout: 5000 })
    cy.wait(1000)

    cy.get('[data-row-key] > :nth-child(' + SortTypeClas + ')').then(items => {

        let SortC = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(SortC)
        cy.get(':nth-child(' + SortTypeClas + ') > .ant-table-header-column > .ant-table-column-sorters > .ant-table-column-title').click({ timeout: 5000 })
        cy.wait(500)

        cy.get('[data-row-key] > :nth-child(' + SortTypeClas + ')').then(items => {
            let SortD = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(SortD)

            let compare = JSON.stringify(SortD) >= JSON.stringify(SortC)
            console.log(compare)

            if (compare) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', '')
            }

            else {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro na ordenação')
            }

            cy.get(':nth-child(' + SortTypeClas + ') > .ant-table-header-column > .ant-table-column-sorters > .ant-table-column-title').click({ timeout: 5000 })

        })

    })
})

// FUNÇÕES DE SWITCH

Cypress.Commands.add('SwitchRun', () => {
    for (var switchV = 1; switchV <= 6; switchV++) {
        cy.ConfigSwitch(switchV)
    }
})

Cypress.Commands.add('ConfigSwitch', (switchV) => {
    console.log(switchV)
    cy.get(':nth-child(' + switchV + ') > .ant-switch', { timeout: 40000 }).click({ timeout: 5000 })
    cy.get(loc.MESSAGE.TOASTIFY, { timeout: 30000 }).should('contain', 'Configuração alterada com sucesso')
    cy.get(loc.MESSAGE.CLOSE).click({ timeout: 5000 })
    cy.get('.ant-list-items > :nth-child(' + switchV + ')').then(items => {

        let Switch0 = items.map((index, html) => Cypress.$(html).text()).get()
        console.log(Switch0)

        cy.get(':nth-child(' + switchV + ') > .ant-switch', { timeout: 40000 }).click({ timeout: 5000 })
        cy.get(loc.MESSAGE.TOASTIFY, { timeout: 40000 }).should('contain', 'Configuração alterada com sucesso')
        cy.get(loc.MESSAGE.CLOSE).click({ timeout: 5000 })

        cy.get('.ant-list-items > :nth-child(' + switchV + ')').then(items => {
            let Switch1 = items.map((index, html) => Cypress.$(html).text()).get()
            console.log(Switch1)

            let compare = JSON.stringify(Switch1) != JSON.stringify(Switch0)
            console.log(compare)

            if (compare) {
                cy.get('.ant-list-items > :nth-child(' + switchV + ')').should('contain', '')
            }

            else {
                cy.get('.ant-list-items > :nth-child(' + switchV + ')').should('contain', 'Erro na ordenação')
            }

        })

    })
})

// FUNÇÕES DE FILTRO

Cypress.Commands.add('filtroEmpresas', (empresaFiltrada) => {
    cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_FILTRO).clear().type(empresaFiltrada)
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_BOTOES.MENU.PRIMEIRO_REGISTRO, { timeout: 5000 }).should('be.visible').click({ force: true })
    cy.get(loc.ERECUPERADOR_ABAS.INFORMACOES, { timeout: 30000 }).should('be.visible')
    cy.get(loc.ERECUPERADOR_ABAS.INFORMACOES).click({ multiple: true })
    cy.wait(1000)
})

Cypress.Commands.add('filtroReceita', (tipoFiltro, txtFiltro, competencia) => {

    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.BTN_FILTRO).should('be.visible').click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODAL).should('be.visible')

    if (tipoFiltro == 'codigo') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CODIGO).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(1)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'descricao') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DESCRIÇÃO).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(2)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'ncm') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.NCM).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(3)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'ex') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.EX).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(4)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'ean') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.EAN).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(5)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'pis') {

        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.TRIBUTACAO_PIS_CAMPO).click()
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.TRIBUTACAO_PIS_DROPDOWN).contains(txtFiltro).click()
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(6)').should('contain', txtFiltro)

    }

    else if (tipoFiltro == 'icms') {

        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.TRIBUTACAO_ICMS_CAMPO).click()
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.TRIBUTACAO_ICMS_DROPDOWN).contains(txtFiltro).click()
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(7)').should('contain', txtFiltro)

    }


    else if (tipoFiltro == 'num') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.N_DOC).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(8)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'modelo') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODELO).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(9)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'data') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.COMPETENCIA).should('contain', competencia)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DIA).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(10)').should('contain', txtFiltro + '/0' + competencia)
    }

    else if (tipoFiltro == 'cfop') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CFOP).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(11)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'cst') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CST_ICMS).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(12)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'chave') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CHAVE).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(14)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'indicador') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.INDICADOR).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(15)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'situacao') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.SITUACAO).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(16)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'quantidade') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.QUANTIDADE).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(17)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'valor') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.VALOR).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(21)').should('contain', txtFiltro)
    }

})

Cypress.Commands.add('filtroReceitaSeg', (tipoFiltro, txtFiltro, competencia) => {

    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.BTN_FILTRO).should('be.visible').click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODAL).should('be.visible')

    if (tipoFiltro == 'codigo') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CODIGO).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(1)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'descricao') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DESCRIÇÃO).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(2)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'ncm') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.NCM).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(3)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'ex') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.EX).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(4)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'ces') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.EAN).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(5)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'ean') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.EAN).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(6)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'pis') {

        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.TRIBUTACAO_PIS_CAMPO).click()
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.TRIBUTACAO_PIS_DROPDOWN).contains(txtFiltro).click()
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(7)').should('contain', txtFiltro)

    }

    else if (tipoFiltro == 'icms') {

        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.TRIBUTACAO_ICMS_CAMPO).click()
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.TRIBUTACAO_ICMS_DROPDOWN).contains(txtFiltro).click()
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(8)').should('contain', txtFiltro)

    }


    else if (tipoFiltro == 'num') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.N_DOC).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(9)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'modelo') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODELO).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(10)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'data') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.COMPETENCIA).should('contain', competencia)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DIA).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(11)').should('contain', txtFiltro + '/0' + competencia)
    }

    else if (tipoFiltro == 'cfop') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CFOP).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(12)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'cst') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CST_ICMS).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(13)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'csosn') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CST_ICMS).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(14)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'chave') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.CHAVE).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(15)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'indicador') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.INDICADOR).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(16)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'situacao') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.SITUACAO).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(17)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'quantidade') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.QUANTIDADE).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(18)').should('contain', txtFiltro)
    }

    else if (tipoFiltro == 'valor') {
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.VALOR).clear().type(txtFiltro)
        cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
        cy.waitLoading()
        cy.get('.ant-table-row > :nth-child(23)').should('contain', txtFiltro)
    }

})

Cypress.Commands.add('filtroCompetencia', (dia0, dia, dia2, dia3, comp, vis) => {
    cy.wait(1000)
    cy.get(loc.ERECUPERADOR_ABAS.RESULTADOS).click({ timeout: 5000 })
    cy.waitLoading()
    cy.get(loc.ERECUPERADOR_RESULTADO.BTN_CALSSIFICAR + ':eq(0)').click()
    cy.get(loc.ERECUPERADOR_RESULTADO.MODAL_CLASSIFICAR.PROSSEGUIR).click()

    cy.get(loc.ERECUPERADOR_RESULTADO.BTN_VISUALIZAR + ':eq(' + vis + ')').click()

    cy.filtroReceita('data', dia0, comp)

    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.BTN_FILTRO).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODAL).should('be.visible')
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.COMPETENCIA).should('contain', comp)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DIA).clear().type(dia)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
    cy.get('.ant-table-row > :nth-child(10)').should('contain', dia + '/0' + comp)

    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.BTN_FILTRO).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODAL).should('be.visible')
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.COMPETENCIA).should('contain', comp)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_LIMPAR).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DIA).type(dia2)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
    cy.get('.ant-table-row > :nth-child(10)').should('contain', dia2 + '/0' + comp)

    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.BTN_FILTRO).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODAL).should('be.visible')
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.COMPETENCIA).should('contain', comp)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_LIMPAR).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DIA).type(dia3)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
    cy.get('.ant-table-row > :nth-child(10)').should('not.contain', dia3 + '/0' + comp)

    cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS).click({ timeout: 5000 })
})

Cypress.Commands.add('filtroCompetenciaSeg', (dia0, dia, dia2, dia3, comp, vis) => {

    cy.get(loc.ERECUPERADOR_RESULTADO.BTN_VISUALIZAR_SEG + ':eq(' + vis + ')').click()

    cy.filtroReceitaSeg('data', dia0, comp)

    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.BTN_FILTRO).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODAL).should('be.visible')
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.COMPETENCIA).should('contain', comp)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DIA).clear().type(dia)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
    cy.get('.ant-table-row > :nth-child(11)').should('contain', dia + '/0' + comp)

    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.BTN_FILTRO).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODAL).should('be.visible')
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.COMPETENCIA).should('contain', comp)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_LIMPAR).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DIA).type(dia2)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
    cy.get('.ant-table-row > :nth-child(11)').should('contain', dia2 + '/0' + comp)

    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.BTN_FILTRO).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.MODAL).should('be.visible')
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.COMPETENCIA).should('contain', comp)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_LIMPAR).click()
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.DIA).type(dia3)
    cy.get(loc.ERECUPERADOR_VISUALIZAR.DETALHAMENTO.FILTRO_MODAL.BTN_FILTRAR).click()
    cy.get('.ant-table-row > :nth-child(11)').should('not.contain', dia3 + '/0' + comp)

    cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS).click({ timeout: 5000 })
})

//TERCEIROS

Cypress.Commands.add('primeiroRegistroTer', () => {

    cy.exclusaoTer('EMPRESA PADRAO')

    cy.get(loc.TERCEIROS.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.P_RAZAO).type('EMPRESA PADRAO')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.P_CNPJ).click().type('{selectall}' + '40.042.485/0001-09')
    cy.get('button').contains('Cadastrar').click({ force: true })

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2).should('contain', 'Empresa cadastrada com Sucesso')

    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.contains('Baixar planilha para importação de dados', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('cadastrarEmpTer', (razao, cnpj, endereco, numero, complemento, bairro, municipio, sigla, cep) => {

    cy.get('.header__cnpj:eq(0)').click({ force: true })
    cy.wait(1000)
    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.TERCEIROS.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.RAZAO).type(razao)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CNPJ).click().type('{selectall}' + cnpj)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.RUA).type(endereco)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.NUMERO).type(numero)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.COMPLETO).type(complemento)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.BAIRRO).type(bairro)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.MUNICIPIO).type(municipio)

    if (sigla != ' ') {
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF).click({ force: true })
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF_2).click({ multiple: true })
        cy.get('[data-value="' + sigla + '"]').click({ force: true })
    }
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CEP).click({ force: true }).type('{selectall}' + cep)
    cy.get('button').contains('Cadastrar').click({ force: true })
    cy.contains('Baixar planilha para importação de dados', { timeout: 360000 }).should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CNPJ).should('not.exist')

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2).should('contain', 'Empresa cadastrada com Sucesso')

    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.contains('Baixar planilha para importação de dados', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('editarEmpTer', (razaoFiltro, razao, cnpj, endereco, numero, complemento, bairro, municipio, sigla, cep) => {

    cy.get('.header__cnpj:eq(0)').click()
    cy.wait(1000)
    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.pesquisarTer(razaoFiltro)

    cy.get(loc.TERCEIROS.EMPRESA.EDITAR_PRIMEIRO).click()
    cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.RAZAO).clear().type(razao)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CNPJ).should('be.disabled').should('have.value', cnpj)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.RUA).clear().type(endereco)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.NUMERO).clear().type(numero)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.COMPLETO).clear().type(complemento)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.BAIRRO).clear().type(bairro)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.MUNICIPIO).clear().type(municipio)

    if (sigla != ' ') {
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF).click({ force: true })
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF_2).click({ multiple: true })
        cy.get('[data-value="' + sigla + '"]').click({ force: true })
    }
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CEP).click({ force: true }).clear().type('{selectall}' + cep)
    cy.get('button').contains('Editar').click({ force: true })
    cy.contains('Baixar planilha para importação de dados', { timeout: 360000 }).should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CNPJ).should('not.exist')

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2).should('contain', 'Empresa atualizada com sucesso!')

    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.contains('Baixar planilha para importação de dados', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('principalTerceiros', () => {
    cy.contains('Faça você mesmo o cálculo das Contribuições para Terceiros com a limitação da base de cálculo em 20 salários mínimos.', { timeout: 360000 }).should('be.visible')
    cy.contains('Carregando empresas...').should('not.exist')
    cy.contains('Faça você mesmo o cálculo das Contribuições para Terceiros com a limitação da base de cálculo em 20 salários mínimos.', { timeout: 360000 }).should('be.visible')
})

Cypress.Commands.add('checkSomaSelic', () => {

    cy.get('.MuiTableBody-root > > :nth-child(7)').then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        let tamanho = emp.length
        // console.log(emp)
        // console.log(emp.length)
        // console.log(emp[tamanho - 1])

        function decimalAdjust(type, value, exp) {
            // Se exp é indefinido ou zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // Se o valor não é um número ou o exp não é inteiro...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Transformando para string
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Transformando de volta
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        if (!Math.round10) {
            Math.round10 = function (value, exp) {
                return decimalAdjust('round', value, exp);
            };
        }

        let calSelic = emp.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
        let somaSelic = Math.round10(calSelic, -2)

        cy.get(':nth-child(2)>>>>>>>>>>>>>>:eq(2)').then(result => {

            var credito = result.text()

            let totalSelic = Number(credito.replace(/[^0-9,]+/g, "").replace(',', '.'))

            console.log('val soma ' + somaSelic)
            console.log('val total ' + totalSelic)

            if (somaSelic != totalSelic) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
            }
        })
    })
})

Cypress.Commands.add('checkSelicAtual', (comp) => {


    for (let row = 1; row <= comp; row++) {

        cy.get(':nth-child(' + row + ') > :nth-child(5):eq(0)').then(CaixaVal => {

            cy.get(':nth-child(' + row + ') > :nth-child(6):eq(0)').then(CaixaSELIC => {

                cy.get(':nth-child(' + row + ') > :nth-child(7):eq(0)').then(CaixaATUAL => {


                    function decimalAdjust(type, value, exp) {
                        // Se exp é indefinido ou zero...
                        if (typeof exp === 'undefined' || +exp === 0) {
                            return Math[type](value);
                        }
                        value = +value;
                        exp = +exp;
                        // Se o valor não é um número ou o exp não é inteiro...
                        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                            return NaN;
                        }
                        // Transformando para string
                        value = value.toString().split('e');
                        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                        // Transformando de volta
                        value = value.toString().split('e');
                        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
                    }

                    if (!Math.round10) {
                        Math.round10 = function (value, exp) {
                            return decimalAdjust('round', value, exp);
                        };
                    }

                    let valCreditoTxt = CaixaVal.text()
                    let SelicTxt = CaixaSELIC.text()
                    let valAtualTxt = CaixaATUAL.text()

                    let valCredito = Number(valCreditoTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let Selic = Number(SelicTxt.replace(/[^0-9.]+/g, ""))
                    let valAtual = Number(valAtualTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = Math.round10(valCredito * ((Selic * 0.01) + 1), -2)


                    console.log(row + ' Valor do Crédito: ' + valCredito)
                    console.log(row + ' SELIC : ' + Selic + ' %')
                    console.log(row + ' Crédito Atualizado SELIC: ' + valAtual)
                    console.log(row + ' Calculo: ' + Calc)

                    if (Calc != valAtual) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add("uploadTer", (ArquivoUpload, ArquivoImportado) => {
    cy.get('.MuiButton-label', { timeout: 100000 }).should('be.visible')
    cy.get('.MuiButton-label').attachFile(ArquivoImportado, { allowEmpty: true, subjectType: 'drag-n-drop' });
    cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(1)', { timeout: 10000 }).should('contain', ArquivoUpload)
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Iniciando')
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Processando')
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('contain', 'Finalizado')

});

Cypress.Commands.add("exclusaoTerNew", (NomeEmpresa) => {

    cy.wait(1000)
    cy.get(':nth-child(2) >>>>>>>>>').should('be.visible').then(Emps => {
        var listEmp = Emps.text()
        var isEmp = listEmp.includes(NomeEmpresa)

        console.log(listEmp)
        console.log(isEmp)

        if (isEmp) {

            cy.pesquisarTer(NomeEmpresa)

            cy.wait(1000)

            cy.get(loc.TERCEIROS.EMPRESA.REMOVER_PRIMEIRO).click({ force: true })
            cy.wait(1000)
            cy.get('button').contains('Apagar').click({ force: true })
            cy.wait(1000)

            cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('contain', 'Empresa removida com sucesso!')

            cy.contains('Carregando empresas...').should('not.exist')

            cy.get(loc.TERCEIROS.MENU.III).click()

            cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

            cy.get(loc.TERCEIROS.MENU.III).click()

            cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

        }
    })
})

Cypress.Commands.add("exclusaoTer", (NomeEmpresa) => {

    cy.wait(1000)
    cy.get(':nth-child(2) >>>>>>>>>').should('be.visible').then(Emps => {
        var listEmp = Emps.text()
        var isEmp = listEmp.includes(NomeEmpresa)

        console.log(listEmp)
        console.log(isEmp)

        if (isEmp) {


            cy.get('[title="Apagar empresa"]:eq(0)').click({ force: true })
            // cy.pesquisarTer(NomeEmpresa) 

            cy.wait(1000)
            cy.get('button').contains('Apagar').click({ force: true })
            cy.wait(1000)
        }
    })
})

Cypress.Commands.add("deleteTer", (deletartodas = false) => {

    cy.get('>>>>>>').should('be.visible').then(Emps => {
        var listEmp = Emps.text()
        var isEmp = listEmp.includes('Sem empresas cadastradas')

        console.log(listEmp)
        console.log(isEmp)

        if (!isEmp) {
            console.log('APAGANDO')
            cy.get('[title="Apagar empresa"]:eq(0)').click({ force: true })
            cy.wait(1000)
            cy.get('button').contains('Apagar').click({ force: true })
            cy.wait(1000)

            cy.contains('Carregando empresas...').should('not.exist')

            cy.get(loc.TERCEIROS.MENU.III).click()

            cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

            cy.get(loc.TERCEIROS.MENU.III).click({ force: true })

            if (deletartodas) {
                cy.deleteTer(true)

            }
        }
    })
})

Cypress.Commands.add("deleteTerAll", () => {
    cy.deleteTer(true)

})

Cypress.Commands.add('pesquisarTer', (NomeEmpresa) => {
    cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear().type(NomeEmpresa)
})

Cypress.Commands.add('crashLogoffTer', () => {
    cy.wait(500)
    cy.url().then(URL => {

        var texto = URL

        if (texto == 'https://contribuicoesterceiros.e-auditoria.com.br/home') {

            cy.waitLoading()

            cy.visit(link, { timeout: 10000, force: true })

            cy.get(loc.LOGIN.USER, { timeout: 30000 }).should('be.visible').type(baseLogada)

            cy.get(loc.LOGIN.PASSWORD).type(senhaPadrao)
            cy.get(loc.LOGIN.BTN_LOGIN, { timeout: 50000 }).click({ force: true })

            cy.get('.title', { timeout: 30000 }).should('be.visible').should('contain', 'Todas as ferramentas que você precisa em um único lugar')
            cy.get(loc.MODULES.ERECUPERADOR, { timeout: 30000 }).should('be.visible').click({ force: true })
            cy.get('.block_tela', { timeout: 50000 }).should('not.exist')

            cy.waitLoading()

            cy.crashContrato()

            cy.acessoTerceiros()

        }


    })
})

Cypress.Commands.add("checkXLSXter", (arquivo, row, column, sheet, checkEqual) => {

    let fileWhere = 'cypress/downloads/'
    let file = fileWhere + arquivo

    cy.get(loc.MESSAGE.POPUP_TERCEIROS, { timeout: 360000 }).should('be.visible').should('contain', 'Relatório gerado com sucesso').then(() => {
        cy.task('readXlsx', { file, sheet }).then((rows) => {
            console.log(rows)
            expect(rows[row][column]).to.equals(checkEqual)


        })
    });
})

Cypress.Commands.add('checkXLSSomaSelic', (arquivo, row, column, sheet) => {

    cy.get(':nth-child(2)>>>>>>>>>>>>>>:eq(2)').then(result => {

        let credito = result.text()
        let totalSelic = credito.replace(/[^0-9.,]+/g, '')

        cy.checkXLSXter(arquivo, row, column, sheet, 'R$ ' + totalSelic)
    })
})

Cypress.Commands.add('crashCancelSeg', () => {
    cy.wait(1000)

    cy.get(':nth-child(1) ').should('be.visible').then(cancel => {
        var botoes = cancel.text()
        var btnCancelar = botoes.includes('Cancelar')

        if (btnCancelar) {
            cy.contains('Cancelar').click({ force: true })
        }
    })
})

//Verbas

Cypress.Commands.add('principalVerbas', () => {
    cy.contains('Faça você mesmo o cálculo do INSS recolhido indevidamente sobre as verbas indenizatórias.', { timeout: 360000 }).should('be.visible')
})

Cypress.Commands.add('cadastrarEmpVer', (razao, cnpj, endereco, numero, complemento, bairro, municipio, sigla, cep) => {

    cy.get(loc.VERBAS.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.VERBAS.MODAL_CADASTRO.RAZAO).type(razao)
    cy.get(loc.VERBAS.MODAL_CADASTRO.CNPJ).click().type('{selectall}' + cnpj)
    cy.get(loc.VERBAS.MODAL_CADASTRO.RUA).type(endereco)
    cy.get(loc.VERBAS.MODAL_CADASTRO.NUMERO).type(numero)
    cy.get(loc.VERBAS.MODAL_CADASTRO.COMPLETO).type(complemento)
    cy.get(loc.VERBAS.MODAL_CADASTRO.BAIRRO).type(bairro)
    cy.get(loc.VERBAS.MODAL_CADASTRO.MUNICIPIO).type(municipio)

    if (sigla = ! ' ') {
        cy.get(loc.VERBAS.MODAL_CADASTRO.DROPDOWN_UF).click({ force: true })
        cy.get(loc.VERBAS.MODAL_CADASTRO.DROPDOWN_UF_2).click({ multiple: true })
        cy.get('[data-value="' + sigla + '"]').click({ force: true })
    }
    cy.get(loc.VERBAS.MODAL_CADASTRO.CEP).click({ force: true }).type('{selectall}' + cep)
    cy.get('button').contains('Cadastrar').click({ force: true })
    cy.contains('Baixar a planilha de dados de trabalhadores', { timeout: 360000 }).should('be.visible')



})

Cypress.Commands.add("uploadVer", (ArquivoUpload, ArquivoImportado) => {
    cy.get('.MuiButton-label:eq(0)', { timeout: 100000 }).should('be.visible')
    cy.get('.MuiButton-label:eq(0)').attachFile(ArquivoImportado, { allowEmpty: true, subjectType: 'drag-n-drop' });
    cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(1)', { timeout: 10000 }).should('contain', ArquivoUpload)
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Iniciando')
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Processando')
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('contain', 'Finalizado')
});

Cypress.Commands.add('waitLoadingVer', () => {
    cy.get('.MuiCircularProgress-svg', { timeout: 50000 }).should('not.exist')
})

Cypress.Commands.add('waitLoadingVer2', () => {
    cy.contains('Carregando resultados', { timeout: 50000 }).should('not.exist')
})

Cypress.Commands.add('waitLoadingFiltro', () => {
    cy.get('.MuiGrid-container:eq(2)', { timeout: 50000 }).should('not.exist')
})

Cypress.Commands.add('filtroSwitchSelect', (cod) => {

    cy.get(loc.VERBAS.SELECAO.MODAL.FILTRO).type(cod)
    cy.contains('1 - 1 de 1').should('be.visible')
    cy.waitLoadingVer()
    cy.get(loc.VERBAS.SELECAO.MODAL.SWITCH_1).click({ force: true })
    cy.get('.MuiInputBase-root > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root').click()
    cy.waitLoadingVer()
    cy.get(loc.MESSAGE.POPUP_VERBAS, { timeout: 10000 }).should('contain', 'Configuração atualizada')

})

Cypress.Commands.add('checkSelicAtualVer', (comp) => {

    for (let row = 0; row < comp; row++) {

        cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(4)' + ':eq(' + row + ')').then(CaixaVal => {

            cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(5)' + ':eq(' + row + ')').then(CaixaSELIC => {

                cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(6)' + ':eq(' + row + ')').then(CaixaATUAL => {


                    function decimalAdjust(type, value, exp) {
                        // Se exp é indefinido ou zero...
                        if (typeof exp === 'undefined' || +exp === 0) {
                            return Math[type](value);
                        }
                        value = +value;
                        exp = +exp;
                        // Se o valor não é um número ou o exp não é inteiro...
                        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                            return NaN;
                        }
                        // Transformando para string
                        value = value.toString().split('e');
                        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                        // Transformando de volta
                        value = value.toString().split('e');
                        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
                    }

                    if (!Math.round10) {
                        Math.round10 = function (value, exp) {
                            return decimalAdjust('round', value, exp);
                        };
                    }

                    let valCreditoTxt = CaixaVal.text()
                    let SelicTxt = CaixaSELIC.text()
                    let valAtualTxt = CaixaATUAL.text()

                    let valCredito = Number(valCreditoTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let Selic = Number(SelicTxt.replace(/[^0-9.]+/g, ""))
                    let valAtual = Number(valAtualTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = Math.round10(valCredito * ((Selic * 0.01) + 1), -2)


                    console.log(row + ' Valor do Crédito: ' + valCredito)
                    console.log(row + ' SELIC : ' + Selic + ' %')
                    console.log(row + ' Crédito Atualizado SELIC: ' + valAtual)
                    console.log(row + ' Calculo: ' + Calc)

                    if (Calc != valAtual) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add('checkSelicAtualVer2', (comp) => {

    for (let row = 0; row < comp; row++) {

        cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(5)' + ':eq(' + row + ')').then(CaixaVal => {

            cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(6)' + ':eq(' + row + ')').then(CaixaSELIC => {

                cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(7)' + ':eq(' + row + ')').then(CaixaATUAL => {


                    function decimalAdjust(type, value, exp) {
                        // Se exp é indefinido ou zero...
                        if (typeof exp === 'undefined' || +exp === 0) {
                            return Math[type](value);
                        }
                        value = +value;
                        exp = +exp;
                        // Se o valor não é um número ou o exp não é inteiro...
                        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                            return NaN;
                        }
                        // Transformando para string
                        value = value.toString().split('e');
                        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                        // Transformando de volta
                        value = value.toString().split('e');
                        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
                    }

                    if (!Math.round10) {
                        Math.round10 = function (value, exp) {
                            return decimalAdjust('round', value, exp);
                        };
                    }

                    let valCreditoTxt = CaixaVal.text()
                    let SelicTxt = CaixaSELIC.text()
                    let valAtualTxt = CaixaATUAL.text()

                    let valCredito = Number(valCreditoTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let Selic = Number(SelicTxt.replace(/[^0-9.]+/g, ""))
                    let valAtual = Number(valAtualTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = Math.round10(valCredito * ((Selic * 0.01) + 1), -2)


                    console.log(row + ' Valor do Crédito: ' + valCredito)
                    console.log(row + ' SELIC : ' + Selic + ' %')
                    console.log(row + ' Crédito Atualizado SELIC: ' + valAtual)
                    console.log(row + ' Calculo: ' + Calc)

                    if (Calc != valAtual) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add('checkSomaSelicVer', () => {

    cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(6)').then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        let tamanho = emp.length
        // console.log(emp)
        // console.log(emp.length)
        // console.log(emp[tamanho - 1])

        function decimalAdjust(type, value, exp) {
            // Se exp é indefinido ou zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // Se o valor não é um número ou o exp não é inteiro...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Transformando para string
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Transformando de volta
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        if (!Math.round10) {
            Math.round10 = function (value, exp) {
                return decimalAdjust('round', value, exp);
            };
        }

        let calSelic = emp.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
        let somaSelic = Math.round10(calSelic, -2)

        cy.get(':nth-child(4):eq(6)').then(result => {

            var credito = result.text()

            let totalSelic = Number(credito.replace(/[^0-9,]+/g, "").replace(',', '.'))

            console.log('val soma ' + somaSelic)
            console.log('val total ' + totalSelic)

            if (somaSelic != totalSelic) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
            }
        })
    })



})

Cypress.Commands.add('checkSomaSelicVer2', () => {

    cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(7)').then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        let tamanho = emp.length
        // console.log(emp)
        // console.log(emp.length)
        // console.log(emp[tamanho - 1])

        function decimalAdjust(type, value, exp) {
            // Se exp é indefinido ou zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // Se o valor não é um número ou o exp não é inteiro...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Transformando para string
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Transformando de volta
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        if (!Math.round10) {
            Math.round10 = function (value, exp) {
                return decimalAdjust('round', value, exp);
            };
        }

        let calSelic = emp.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
        let somaSelic = Math.round10(calSelic, -2)

        cy.get(':nth-child(4):eq(6)').then(result => {

            var credito = result.text()

            let totalSelic = Number(credito.replace(/[^0-9,]+/g, "").replace(',', '.'))

            console.log('val soma ' + somaSelic)
            console.log('val total ' + totalSelic)

            if (somaSelic != totalSelic) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
            }
        })
    })



})

Cypress.Commands.add("checkXLSXVer", (arquivo, row, column, sheet, checkEqual) => {

    let fileWhere = 'cypress/downloads/'
    let file = fileWhere + arquivo

    cy.get(':nth-child(4):eq(6)').should('exist').then(() => {
        cy.task('readXlsx', { file, sheet }).then((rows) => {
            console.log(rows)
            expect(rows[row][column]).to.equals(checkEqual)


        })
    });
})

Cypress.Commands.add('checkXLSSomaSelicVer', (arquivo, row, column, sheet) => {

    cy.get(':nth-child(4):eq(6)', { timeout: 360000 }).should('exist').then(() => {
        cy.get(':nth-child(4):eq(6)').then(result => {

            let credito = result.text()
            let totalSelic = credito.replace(/[^0-9.,]+/g, '')

            cy.checkXLSXVer(arquivo, row, column, sheet, 'R$ ' + totalSelic)
        })
    })

})

Cypress.Commands.add('auxilioSwitchSelectMANAD', () => {
    cy.get(loc.VERBAS.SELECAO.AUXILIO).click({ force: true })
    cy.contains('Selecionando rubrica: Auxílio-doença/acidente - Até 15 dias', { timeout: 8000 }).should('be.visible')
    cy.wait(1000)
    cy.filtroSwitchSelect('12')
    cy.filtroSwitchSelect('13 SALARIO 1ª PARC.')
    cy.filtroSwitchSelect('VALE TRANSPORTE')
    cy.filtroSwitchSelect('LIQUIDO RESCISAO')
    cy.filtroSwitchSelect('150')
    cy.filtroSwitchSelect('156')
    cy.filtroSwitchSelect('DESC QUEBRA DE CAIXA')
    cy.filtroSwitchSelect('800')
    cy.waitLoadingVer()
    cy.get(loc.VERBAS.SELECAO.MODAL.X).click()
    cy.wait(2000)

    cy.get(loc.VERBAS.SELECAO.SWITCHS.AUXILIO).click()
    cy.waitLoadingVer()

    cy.get(loc.VERBAS.SELECAO.OUTRAS).click({ force: true })
    cy.contains('Selecionando rubrica: Outras verbas', { timeout: 8000 }).should('be.visible')
    cy.wait(1000)
    cy.filtroSwitchSelect('29')
    cy.waitLoadingVer()
    cy.get(loc.VERBAS.SELECAO.MODAL.X).click()
    cy.wait(2000)

    cy.get(loc.VERBAS.SELECAO.SWITCHS.OUTROS).click()
    cy.waitLoadingVer()

})

Cypress.Commands.add('auxilioSwitchSelectESOCIAL', () => {
    cy.get(loc.VERBAS.SELECAO.AUXILIO).click({ force: true })
    cy.contains('Selecionando rubrica: Auxílio-doença/acidente - Até 15 dias', { timeout: 8000 }).should('be.visible')
    cy.wait(1000)
    cy.filtroSwitchSelect('218')
    cy.filtroSwitchSelect('212')
    cy.waitLoadingVer()
    cy.get(loc.VERBAS.SELECAO.MODAL.X).click()
    cy.wait(2000)

    cy.get(loc.VERBAS.SELECAO.SWITCHS.AUXILIO).click()
    cy.waitLoadingVer()

})

Cypress.Commands.add('auxilioSwitchSelectPLANILHA', () => {
    cy.get(loc.VERBAS.SELECAO.MATERNIDADE).click({ force: true })
    cy.contains('Selecionando rubrica: Salário Maternidade', { timeout: 8000 }).should('be.visible')
    cy.wait(1000)
    cy.get(loc.VERBAS.SELECAO.MODAL.SWITCH_1).should('be.visible').click()
    cy.waitLoadingVer()
    cy.get(loc.VERBAS.SELECAO.MODAL.X).click()
    cy.wait(2000)
    cy.get(loc.VERBAS.SELECAO.SWITCHS.MATERNIDADE).click()

})

Cypress.Commands.add("uploadLoopXMLVer", (init, fin, caminhoComp, ArquivoXML, ext) => {
    cy.get('.MuiButton-label:eq(0)', { timeout: 100000 }).should('be.visible')
    for (var varXml = init; varXml <= fin; varXml++) {

        let ArquivoXMLName = ArquivoXML + '(' + varXml + ')' + ext
        let ArquivoXML_IMP = caminhoComp + ArquivoXML + '(' + varXml + ')' + ext

        cy.get('.MuiButton-label:eq(0)', { timeout: 100000 }).should('be.visible')
        cy.get('.MuiButton-label:eq(0)').attachFile(ArquivoXML_IMP, { allowEmpty: true, subjectType: 'drag-n-drop' });
        cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(1)', { timeout: 10000 }).should('contain', ArquivoXMLName)
        cy.get(loc.MESSAGE.POPUP_VERBAS_2, { timeout: 8000 }).should('contain', 'Upload de arquivos realizado com sucesso!')
        cy.wait(1500)
    }

    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Iniciando')
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Processando')
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('contain', 'Finalizado')

    cy.get(loc.VERBAS.ABAS_EMPRESA.RUBRICAS).click()
    cy.waitLoadingVer2()
    cy.get(loc.VERBAS.ABAS_EMPRESA.DOCUMENTOS).click()
    cy.waitLoadingVer2()
    cy.get('.MuiToolbar-root > :nth-child(4)').should('contain', fin)
});

//ISSQN

Cypress.Commands.add('principalISSQN', () => {
    cy.contains('Faça você mesmo o cálculo do PIS e da COFINS com a exclusão do ISSQN de sua base de cálculo.', { timeout: 360000 }).should('be.visible')
})

Cypress.Commands.add('cadastrarEmpISSQN', (razao, cnpj, endereco, numero, complemento, bairro, municipio, sigla, cep) => {

    cy.get(loc.VERBAS.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.VERBAS.MODAL_CADASTRO.RAZAO).type(razao)
    cy.get(loc.VERBAS.MODAL_CADASTRO.CNPJ).click().type('{selectall}' + cnpj)
    cy.get(loc.VERBAS.MODAL_CADASTRO.RUA).type(endereco)
    cy.get(loc.VERBAS.MODAL_CADASTRO.NUMERO).type(numero)
    cy.get(loc.VERBAS.MODAL_CADASTRO.COMPLETO).type(complemento)
    cy.get(loc.VERBAS.MODAL_CADASTRO.BAIRRO).type(bairro)
    cy.get(loc.VERBAS.MODAL_CADASTRO.MUNICIPIO).type(municipio)

    if (sigla = ! ' ') {
        cy.get(loc.VERBAS.MODAL_CADASTRO.DROPDOWN_UF).click({ force: true })
        cy.get(loc.VERBAS.MODAL_CADASTRO.DROPDOWN_UF_2).click({ multiple: true })
        cy.get('[data-value="' + sigla + '"]').click({ force: true })
    }
    cy.get(loc.VERBAS.MODAL_CADASTRO.CEP).click({ force: true }).type('{selectall}' + cep)
    cy.get('button').contains('Cadastrar').click({ force: true })
    cy.contains('Baixar a planilha de dados para o cálculo da exclusão do ISSQN da base de cálculo do PIS/COFINS', { timeout: 360000 }).should('be.visible')
})

Cypress.Commands.add("uploadISSQN", (ArquivoUpload, ArquivoImportado) => {
    cy.get('.MuiButton-label:eq(0)', { timeout: 100000 }).should('be.visible')
    cy.get('.MuiButton-label:eq(0)').attachFile(ArquivoImportado, { allowEmpty: true, subjectType: 'drag-n-drop' });
    cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(1)', { timeout: 10000 }).should('contain', ArquivoUpload)
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Iniciando')
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Processando')
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('contain', 'Finalizado')
});

Cypress.Commands.add('configSwitchSelectPlanilhaISSQN', () => {
    cy.get(loc.ISSQN.ABAS_EMPRESA.CONFIGURACOES).click()
    cy.waitLoadingVer()
    cy.get(loc.ISSQN.CONFIGURACOES.SWITCHS.PLANILHA).click()
    cy.get(loc.ISSQN.MODAL.PROSSEGUIR).click()
    cy.waitLoading()
    cy.get(loc.MESSAGE.POPUP_ISSQN).should('contain', 'Configuração alterada com sucesso')
});

Cypress.Commands.add('configSwitchSelectBLOCOMISSQN', () => {
    cy.get(loc.ISSQN.ABAS_EMPRESA.CONFIGURACOES).click()
    cy.waitLoadingVer()
    cy.get(loc.ISSQN.CONFIGURACOES.SWITCHS.BROCOM).click()
    cy.get(loc.ISSQN.MODAL.PROSSEGUIR).click()
    cy.waitLoading()
    cy.get(loc.MESSAGE.POPUP_ISSQN).should('contain', 'Configuração alterada com sucesso')
});

Cypress.Commands.add('crashContratoISSQN', () => {



    cy.wait(1000)
    cy.get('.MuiTypography-root').should('be.visible').then(contrato => {
        var botoes = contrato.text()
        var ccontrato = botoes.includes('Eu li e concordo com o contrato de uso')

        console.log(botoes)

        if (ccontrato) {
            cy.contains('Eu li e concordo com o contrato de uso').should('be.visible')
            cy.get('input').check()
            cy.contains('Aceito').click()
        }
    })


});

Cypress.Commands.add('checkSelicAtualISSQN_PIS', (comp) => {

    for (let row = 0; row < comp; row++) {

        cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(7)' + ':eq(' + row + ')').then(CaixaVal => {

            cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(8)' + ':eq(' + row + ')').then(CaixaSELIC => {

                cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(9)' + ':eq(' + row + ')').then(CaixaATUAL => {


                    function decimalAdjust(type, value, exp) {
                        // Se exp é indefinido ou zero...
                        if (typeof exp === 'undefined' || +exp === 0) {
                            return Math[type](value);
                        }
                        value = +value;
                        exp = +exp;
                        // Se o valor não é um número ou o exp não é inteiro...
                        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                            return NaN;
                        }
                        // Transformando para string
                        value = value.toString().split('e');
                        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                        // Transformando de volta
                        value = value.toString().split('e');
                        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
                    }

                    if (!Math.round10) {
                        Math.round10 = function (value, exp) {
                            return decimalAdjust('round', value, exp);
                        };
                    }

                    let valCreditoTxt = CaixaVal.text()
                    let SelicTxt = CaixaSELIC.text()
                    let valAtualTxt = CaixaATUAL.text()

                    let valCredito = Number(valCreditoTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let Selic = Number(SelicTxt.replace(/[^0-9.]+/g, ""))
                    let valAtual = Number(valAtualTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = Math.round10(valCredito * ((Selic * 0.01) + 1), -2)


                    console.log(row + ' Valor do Crédito: ' + valCredito)
                    console.log(row + ' SELIC : ' + Selic + ' %')
                    console.log(row + ' Crédito Atualizado SELIC: ' + valAtual)
                    console.log(row + ' Calculo: ' + Calc)

                    if (Calc != valAtual) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add('checkSomaSelicISSQN_PIS', () => {

    cy.get('.MuiTableBody-root > .MuiTableRow-root > :nth-child(9)').then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        let tamanho = emp.length
        // console.log(emp)
        // console.log(emp.length)
        // console.log(emp[tamanho - 1])

        function decimalAdjust(type, value, exp) {
            // Se exp é indefinido ou zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // Se o valor não é um número ou o exp não é inteiro...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Transformando para string
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Transformando de volta
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        if (!Math.round10) {
            Math.round10 = function (value, exp) {
                return decimalAdjust('round', value, exp);
            };
        }

        let calSelic = emp.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
        let somaSelic = Math.round10(calSelic, -2)

        cy.get(':nth-child(2)>>>>>>>>>>>>>>>:eq(1)').then(result => {

            var credito = result.text()

            let totalSelic = Number(credito.replace(/[^0-9,]+/g, "").replace(',', '.'))

            console.log('val soma ' + somaSelic)
            console.log('val total ' + totalSelic)

            if (somaSelic != totalSelic) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
            }
        })
    })



})

Cypress.Commands.add('checkXLSSomaSelicISSQN_PIS', (arquivo, row, column, sheet) => {

    cy.get(':nth-child(2)>>>>>>>>>>>>>>>:eq(1)', { timeout: 360000 }).should('exist').then(() => {
        cy.get(':nth-child(2)>>>>>>>>>>>>>>>:eq(1)').then(result => {

            let credito = result.text()
            let totalSelic = credito.replace(/[^0-9.,]+/g, '')

            cy.checkXLSXVer(arquivo, row, column, sheet, 'R$ ' + totalSelic)
        })
    })

})


//DIFAL

Cypress.Commands.add('principalDIFAL', () => {
    cy.contains('Faça você mesmo o cálculo do DIFAL indevidamente recolhido por empresas optantes pelo Simples Nacional', { timeout: 360000 }).should('be.visible')
})

Cypress.Commands.add('primeiroRegistroDIFAL', () => {

    cy.exclusaoTer('EMPRESA PADRAO')

    cy.get(loc.DIFAL.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.DIFAL.MODAL_CADASTRO.P_RAZAO).type('EMPRESA PADRAO')
    cy.get(loc.DIFAL.MODAL_CADASTRO.P_CNPJ).click().type('{selectall}' + '40.042.485/0001-09')
    cy.get('button').contains('Cadastrar').click({ force: true })

    cy.get(loc.MESSAGE.POPUP_DIFAL_2).should('contain', 'Empresa cadastrada com Sucesso')

    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.DIFAL.MENU.III).click()

    cy.get(loc.MESSAGE.POPUP_DIFAL_2, { timeout: 36000 }).should('not.exist')

    cy.get(loc.DIFAL.MENU.III).click()

    cy.contains('Baixar a planilha para importação de dados', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('cadastrarEmpDIFAL', (razao, cnpj, endereco, numero, complemento, bairro, municipio, sigla, cep) => {

    cy.get('.header__cnpj:eq(0)').click({ force: true })
    cy.wait(1000)
    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.TERCEIROS.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.RAZAO_2).type(razao)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CNPJ_2).click().type('{selectall}' + cnpj)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.RUA_2).type(endereco)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.NUMERO_2).type(numero)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.COMPLETO_2).type(complemento)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.BAIRRO_2).type(bairro)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.MUNICIPIO_2).type(municipio)

    if (sigla != ' ') {
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF_X).click({ force: true })
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF_X2).click({ multiple: true })
        cy.get('[data-value="' + sigla + '"]').click({ force: true })
    }
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CEP_2).click({ force: true }).type('{selectall}' + cep)
    cy.get('button').contains('Cadastrar').click({ force: true })
    cy.contains('Baixar a planilha para importação de dados', { timeout: 360000 }).should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CNPJ).should('not.exist')

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2).should('contain', 'Empresa cadastrada com Sucesso')

    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.contains('Baixar a planilha para importação de dados', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('editarEmpDIFAL', (razaoFiltro, razao, cnpj, endereco, numero, complemento, bairro, municipio, sigla, cep) => {

    cy.get('.header__cnpj:eq(0)').click()
    cy.wait(1000)
    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.DIFAL.MENU.III).click()

    cy.pesquisarTer(razaoFiltro)

    cy.get(loc.DIFAL.EMPRESA.EDITAR_PRIMEIRO).click()
    cy.get(loc.DIFAL.EMPRESA.FILTRO_EMPRESAS).clear()

    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.DIFAL.MODAL_CADASTRO.RAZAO).clear().type(razao)
    cy.get(loc.DIFAL.MODAL_CADASTRO.CNPJ).should('be.disabled').should('have.value', cnpj)
    cy.get(loc.DIFAL.MODAL_CADASTRO.RUA).clear().type(endereco)
    cy.get(loc.DIFAL.MODAL_CADASTRO.NUMERO).clear().type(numero)
    cy.get(loc.DIFAL.MODAL_CADASTRO.COMPLETO).clear().type(complemento)
    cy.get(loc.DIFAL.MODAL_CADASTRO.BAIRRO).clear().type(bairro)
    cy.get(loc.DIFAL.MODAL_CADASTRO.MUNICIPIO).clear().type(municipio)

    if (sigla != ' ') {
        cy.get(loc.DIFAL.MODAL_CADASTRO.DROPDOWN_UF).click({ force: true })
        cy.get(loc.DIFAL.MODAL_CADASTRO.DROPDOWN_UF_2).click({ multiple: true })
        cy.get('[data-value="' + sigla + '"]').click({ force: true })
    }
    cy.get(loc.DIFAL.MODAL_CADASTRO.CEP).click({ force: true }).clear().type('{selectall}' + cep)
    cy.get('button').contains('Editar').click({ force: true })
    cy.contains('Faça você mesmo o cálculo do DIFAL indevidamente recolhido por empresas optantes pelo Simples Nacional', { timeout: 360000 }).should('be.visible')
    cy.get(loc.DIFAL.MODAL_CADASTRO.CNPJ).should('not.exist')

    cy.get(loc.MESSAGE.POPUP_DIFAL_2).should('contain', 'Empresa atualizada com sucesso!')

    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.DIFAL.MENU.III).click()

    cy.get(loc.MESSAGE.POPUP_DIFAL_2, { timeout: 36000 }).should('not.exist')

    cy.get(loc.DIFAL.MENU.III).click()

    cy.contains('Faça você mesmo o cálculo do DIFAL indevidamente recolhido por empresas optantes pelo Simples Nacional', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('checkSomaSelicDIFAL', () => {

    cy.get('.MuiTableBody-root > > :nth-child(7)').then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        let tamanho = emp.length
        // console.log(emp)
        // console.log(emp.length)
        // console.log(emp[tamanho - 1])

        function decimalAdjust(type, value, exp) {
            // Se exp é indefinido ou zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // Se o valor não é um número ou o exp não é inteiro...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Transformando para string
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Transformando de volta
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        if (!Math.round10) {
            Math.round10 = function (value, exp) {
                return decimalAdjust('round', value, exp);
            };
        }

        let calSelic = emp.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
        let somaSelic = Math.round10(calSelic, -2)

        cy.get(':nth-child(2)>>>>>>>>>>>>>>:eq(2)').then(result => {

            var credito = result.text()

            let totalSelic = Number(credito.replace(/[^0-9,]+/g, "").replace(',', '.'))

            console.log('val soma ' + somaSelic)
            console.log('val total ' + totalSelic)

            if (somaSelic != totalSelic) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
            }
        })
    })
})

Cypress.Commands.add('checkSelicAtualDIFAL', (comp) => {


    for (let row = 1; row <= comp; row++) {

        cy.get(':nth-child(' + row + ') > :nth-child(5):eq(0)').then(CaixaVal => {

            cy.get(':nth-child(' + row + ') > :nth-child(6):eq(0)').then(CaixaSELIC => {

                cy.get(':nth-child(' + row + ') > :nth-child(7):eq(0)').then(CaixaATUAL => {


                    function decimalAdjust(type, value, exp) {
                        // Se exp é indefinido ou zero...
                        if (typeof exp === 'undefined' || +exp === 0) {
                            return Math[type](value);
                        }
                        value = +value;
                        exp = +exp;
                        // Se o valor não é um número ou o exp não é inteiro...
                        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                            return NaN;
                        }
                        // Transformando para string
                        value = value.toString().split('e');
                        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                        // Transformando de volta
                        value = value.toString().split('e');
                        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
                    }

                    if (!Math.round10) {
                        Math.round10 = function (value, exp) {
                            return decimalAdjust('round', value, exp);
                        };
                    }

                    let valCreditoTxt = CaixaVal.text()
                    let SelicTxt = CaixaSELIC.text()
                    let valAtualTxt = CaixaATUAL.text()

                    let valCredito = Number(valCreditoTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let Selic = Number(SelicTxt.replace(/[^0-9.]+/g, ""))
                    let valAtual = Number(valAtualTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = Math.round10(valCredito * ((Selic * 0.01) + 1), -2)


                    console.log(row + ' Valor do Crédito: ' + valCredito)
                    console.log(row + ' SELIC : ' + Selic + ' %')
                    console.log(row + ' Crédito Atualizado SELIC: ' + valAtual)
                    console.log(row + ' Calculo: ' + Calc)

                    if (Calc != valAtual) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add("uploadDIFAL", (ArquivoUpload, ArquivoImportado) => {

    ///CRIAR FUNÇÃO DE ATUALIZAÇÃO AUTOMATICA




    cy.get('.MuiButton-label:eq(0)', { timeout: 100000 }).should('be.visible')
    cy.get('.MuiButton-label:eq(0)').attachFile(ArquivoImportado, { allowEmpty: true, subjectType: 'drag-n-drop' });

    cy.get(loc.MESSAGE.POPUP_DIFAL_2).should('contain', 'Upload de arquivos realizado com sucesso!')

    cy.get(':nth-child(1) ').should('be.visible').then(cancel => {
        var botoes = cancel.text()
        var btnCancelar = botoes.includes('Cancelar')

        if (btnCancelar) {
            cy.contains('Cancelar').click({ force: true })
        }
    })

    cy.get(loc.DIFAL.ABAS_EMPRESA.RESULTADOS).click()

    cy.contains('Carregando resultados').should('not.exist')

    cy.get(loc.DIFAL.ABAS_EMPRESA.DOCUMENTOS).click()

    cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(1)', { timeout: 10000 }).should('contain', ArquivoUpload)

    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('contain', 'Aguardando Identificação')

    cy.get(loc.DIFAL.ABAS_EMPRESA.RESULTADOS).click()

    cy.contains('Carregando resultados').should('not.exist')

    cy.get(loc.DIFAL.ABAS_EMPRESA.DOCUMENTOS).click()

    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('not.contain', 'Processando')


    // cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 360000 }).should('contain', 'Finalizado')

});

Cypress.Commands.add("exclusaoDIFALNew", (NomeEmpresa) => {

    cy.wait(1000)
    cy.get(':nth-child(2) >>>>>>>>>').should('be.visible').then(Emps => {
        var listEmp = Emps.text()
        var isEmp = listEmp.includes(NomeEmpresa)

        console.log(listEmp)
        console.log(isEmp)

        if (isEmp) {

            cy.pesquisarTer(NomeEmpresa)

            cy.wait(1000)

            cy.get(loc.DIFAL.EMPRESA.REMOVER_PRIMEIRO).click({ force: true })
            cy.wait(1000)
            cy.get('button').contains('Apagar').click({ force: true })
            cy.wait(1000)

            cy.get(loc.MESSAGE.POPUP_DIFAL_2).should('contain', 'Empresa atualizada com sucesso!')

            cy.contains('Carregando empresas...').should('not.exist')

            cy.get(loc.DIFAL.MENU.III).click()

            cy.get(loc.MESSAGE.POPUP_DIFAL_2, { timeout: 36000 }).should('not.exist')

            cy.get(loc.DIFAL.MENU.III).click()

            cy.contains('Faça você mesmo o cálculo do DIFAL indevidamente recolhido por empresas optantes pelo Simples Nacional', { timeout: 360000 }).should('be.visible')

            cy.get(loc.DIFAL.EMPRESA.FILTRO_EMPRESAS).clear()

        }
    })
})

Cypress.Commands.add("deleteDIFAL", (deletartodas = false) => {

    cy.get('>>>>>>').should('be.visible').then(Emps => {
        var listEmp = Emps.text()
        var isEmp = listEmp.includes('Sem empresas cadastradas')

        console.log(listEmp)
        console.log(isEmp)

        if (!isEmp) {
            console.log('APAGANDO')
            cy.get('[title="Apagar empresa"]:eq(0)').click({ force: true })
            cy.wait(1000)
            cy.get('button').contains('Apagar').click({ force: true })
            cy.wait(1000)

            cy.contains('Carregando empresas...').should('not.exist')

            cy.get(loc.DIFAL.MENU.III).click()

            cy.get(loc.MESSAGE.POPUP_DIFAL_2, { timeout: 36000 }).should('not.exist')

            cy.get(loc.DIFAL.MENU.III).click()

            cy.contains('Faça você mesmo o cálculo do DIFAL indevidamente recolhido por empresas optantes pelo Simples Nacional', { timeout: 360000 }).should('be.visible')

            if (deletartodas) {
                cy.deleteTer(true)

            }
        }
    })
})

Cypress.Commands.add("deleteDIFALAll", () => {
    cy.deleteDIFAL(true)
})

Cypress.Commands.add('pesquisarDIFAL', (NomeEmpresa) => {
    cy.get(loc.DIFAL.EMPRESA.FILTRO_EMPRESAS).type(NomeEmpresa)
})

Cypress.Commands.add('crashLogoffDIFAL', () => {
    cy.wait(500)
    cy.url().then(URL => {

        var texto = URL

        if (texto == 'https://contribuicoesterceiros.e-auditoria.com.br/home') {

            cy.waitLoading()

            cy.visit(link, { timeout: 10000, force: true })

            cy.get(loc.LOGIN.USER, { timeout: 30000 }).should('be.visible').type(baseLogada)

            cy.get(loc.LOGIN.PASSWORD).type(senhaPadrao)
            cy.get(loc.LOGIN.BTN_LOGIN, { timeout: 50000 }).click({ force: true })

            cy.get('.title', { timeout: 30000 }).should('be.visible').should('contain', 'Todas as ferramentas que você precisa em um único lugar')
            cy.get(loc.MODULES.ERECUPERADOR, { timeout: 30000 }).should('be.visible').click({ force: true })
            cy.get('.block_tela', { timeout: 50000 }).should('not.exist')

            cy.waitLoading()

            cy.crashContrato()

            cy.acessoTerceiros()

        }


    })
})

Cypress.Commands.add("checkXLSXDIFAL", (arquivo, row, column, sheet, checkEqual) => {

    let fileWhere = 'cypress/downloads/'
    let file = fileWhere + arquivo

    cy.get(loc.MESSAGE.POPUP_DIFAL, { timeout: 360000 }).should('be.visible').should('contain', 'Relatório gerado com sucesso').then(() => {
        cy.task('readXlsx', { file, sheet }).then((rows) => {
            console.log(rows)
            expect(rows[row][column]).to.equals(checkEqual)


        })
    });
})

Cypress.Commands.add('checkXLSSomaSelicDIFAL', (arquivo, row, column, sheet) => {

    cy.get(':nth-child(2)>>>>>>>>>>>>>>:eq(2)').then(result => {

        let credito = result.text()
        let totalSelic = credito.replace(/[^0-9.,]+/g, '')

        cy.checkXLSXter(arquivo, row, column, sheet, 'R$ ' + totalSelic)
    })
})





























//ICMS

Cypress.Commands.add('principalICMS', () => {
    cy.contains('Faça você mesmo o cálculo do PIS e da COFINS com a exclusão do ICMS de sua base de cálculo.', { timeout: 360000 }).should('be.visible')
})

Cypress.Commands.add('primeiroRegistroICMS', () => {

    cy.exclusaoTer('AAEMPRESA PADRAO')

    cy.get(loc.TERCEIROS.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.P_RAZAO).type('AAEMPRESA PADRAO')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.P_CNPJ).click().type('{selectall}' + '40.042.485/0001-09')
    cy.get('button').contains('Cadastrar').click({ force: true })

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2).should('contain', 'Empresa cadastrada com Sucesso')

    cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')

    cy.contains('Carregando empresas...').should('not.exist')


    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('be.visible')

    cy.get('.MuiIconButton-label > .MuiSvgIcon-root').click()

    // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

    cy.contains('Solte ou clique para enviar arquivos', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('cadastrarEmpICMS', (razao, cnpj, endereco, numero, complemento, bairro, municipio, sigla, cep) => {


    cy.contains('EMPRESA PADRAO').click({ force: true })

    cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')
    cy.wait(1000)
    cy.contains('Carregando empresas...').should('not.exist')

    cy.crashContratoICMS2()

    cy.get(loc.TERCEIROS.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.RAZAO_2).type(razao)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CNPJ_2).click().type('{selectall}' + cnpj)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.RUA_2).type(endereco)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.NUMERO_2).type(numero)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.COMPLETO_2).type(complemento)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.BAIRRO_2).type(bairro)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.MUNICIPIO_2).type(municipio)

    if (sigla != ' ') {
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF_X).click({ force: true })
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF_X2).click({ multiple: true })
        cy.get('[data-value="' + sigla + '"]').click({ force: true })
    }
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.CEP_2).click({ force: true }).type('{selectall}' + cep)
    cy.get('button').contains('Cadastrar').click({ force: true })
    cy.contains('Solte ou clique para enviar arquivos', { timeout: 360000 }).should('be.visible')
    cy.get(loc.ICMS.MODAL_CADASTRO.CNPJ, { timeout: 15000 }).should('not.exist')

    // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2).should('contain', 'Empresa cadastrada com Sucesso')

    cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')

    cy.contains('Carregando empresas...').should('not.exist')

    // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('be.visible')

    cy.get('.MuiIconButton-label > .MuiSvgIcon-root').click()

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

    // cy.get(loc.TERCEIROS.MENU.III).click()

    cy.selecioanarICMS('40.042.485/0001-09')

    cy.wait(1500)

    cy.selecioanarICMS(cnpj)

    cy.contains('Solte ou clique para enviar arquivos', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('editarEmpICMS', (razaoFiltro, razao, cnpj, endereco, numero, complemento, bairro, municipio, sigla, cep) => {

    cy.get('.header__cnpj:eq(0)').click()

    cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')
    cy.wait(1000)
    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.TERCEIROS.MENU.III).click()

    cy.pesquisarTer(razaoFiltro)

    cy.get(loc.TERCEIROS.EMPRESA.EDITAR_PRIMEIRO).click()
    cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

    cy.get('#modal-empresa').should('be.visible')

    cy.get(loc.TERCEIROS.MODAL_CADASTRO.EDIT_RAZAO).clear().type(razao)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.EDIT_CNPJ).should('be.disabled').should('have.value', cnpj)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.EDIT_RUA).clear().type(endereco)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.EDIT_NUMERO).clear().type(numero)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.EDIT_COMPLETO).clear().type(complemento)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.EDIT_BAIRRO).clear().type(bairro)
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.EDIT_MUNICIPIO).clear().type(municipio)

    if (sigla != ' ') {
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF_X).click({ force: true })
        cy.get(loc.TERCEIROS.MODAL_CADASTRO.DROPDOWN_UF_X2).click({ multiple: true })
        cy.get('[data-value="' + sigla + '"]').click({ force: true })
    }
    cy.get(loc.TERCEIROS.MODAL_CADASTRO.EDIT_CEP).click({ force: true }).type('{selectall}' + cep)
    cy.get('button').contains('Editar').click({ force: true })
    cy.contains('Solte ou clique para enviar arquivos', { timeout: 360000 }).should('be.visible')
    cy.get(loc.ICMS.MODAL_CADASTRO.CNPJ, { timeout: 15000 }).should('not.exist')

    // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2).should('contain', 'Empresa cadastrada com Sucesso')

    cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')

    cy.contains('Carregando empresas...').should('not.exist')

    // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('be.visible')

    cy.get('.MuiIconButton-label > .MuiSvgIcon-root').click()

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

    // cy.get(loc.TERCEIROS.MENU.III).click()

    cy.selecioanarICMS('40.042.485/0001-09')

    cy.wait(1500)

    cy.selecioanarICMS(cnpj)

    cy.contains('Solte ou clique para enviar arquivos', { timeout: 360000 }).should('be.visible')

});

Cypress.Commands.add('cadastrarEmpICMSBasico', (razao, cnpj) => {

    cy.get('.header__cnpj:eq(0)').click({ force: true })

    cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')
    cy.wait(1000)
    cy.contains('Carregando empresas...').should('not.exist')

    cy.get(loc.ICMS.MENU.CADASTRAR).click({ force: true })
    cy.get('#modal-empresa').should('be.visible')
    cy.get(loc.ICMS.MODAL_CADASTRO.RAZAO_1).type(razao)
    cy.get(loc.ICMS.MODAL_CADASTRO.RAZAO_2).type(razao)
    cy.get(loc.ICMS.MODAL_CADASTRO.CNPJ_2).click().type('{selectall}' + cnpj)

    cy.get('button').contains('Cadastrar').click({ force: true })
    cy.contains('Solte ou clique para enviar arquivos', { timeout: 360000 }).should('be.visible')
    cy.get(loc.ICMS.MODAL_CADASTRO.CNPJ, { timeout: 15000 }).should('not.exist')

    // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2).should('contain', 'Empresa cadastrada com Sucesso')

    cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')

    cy.contains('Carregando empresas...').should('not.exist')

    // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('be.visible')

    cy.get('.MuiIconButton-label > .MuiSvgIcon-root').click()

    cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

    cy.selecioanarICMS('40.042.485/0001-09')

    cy.wait(1500)

    cy.selecioanarICMS(cnpj)

    cy.contains('Solte ou clique para enviar arquivos', { timeout: 360000 }).should('be.visible')

})

Cypress.Commands.add('checkSomaSelicICMS', () => {

    cy.get('.MuiTableBody-root > > :nth-child(7)').then(items => {

        let emp = items.map((index, html) => Cypress.$(html).text()).get()
        let tamanho = emp.length
        // console.log(emp)
        // console.log(emp.length)
        // console.log(emp[tamanho - 1])

        function decimalAdjust(type, value, exp) {
            // Se exp é indefinido ou zero...
            if (typeof exp === 'undefined' || +exp === 0) {
                return Math[type](value);
            }
            value = +value;
            exp = +exp;
            // Se o valor não é um número ou o exp não é inteiro...
            if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                return NaN;
            }
            // Transformando para string
            value = value.toString().split('e');
            value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
            // Transformando de volta
            value = value.toString().split('e');
            return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
        }

        if (!Math.round10) {
            Math.round10 = function (value, exp) {
                return decimalAdjust('round', value, exp);
            };
        }

        let calSelic = emp.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
        let somaSelic = Math.round10(calSelic, -2)

        cy.get(':nth-child(2)>>>>>>>>>>>>>>:eq(2)').then(result => {

            var credito = result.text()

            let totalSelic = Number(credito.replace(/[^0-9,]+/g, "").replace(',', '.'))

            console.log('val soma ' + somaSelic)
            console.log('val total ' + totalSelic)

            if (somaSelic != totalSelic) {
                cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
            }
        })
    })
})

Cypress.Commands.add("uploadICMS", (ArquivoUpload, ArquivoImportado) => {
    cy.get('.MuiSvgIcon-root>:eq(0)', { timeout: 100000 }).should('be.visible')

    cy.contains('Solte ou clique para enviar arquivos').attachFile(ArquivoImportado, { allowEmpty: true, subjectType: 'drag-n-drop' });

    cy.get('.MuiBackdrop-root').should('contain', '0%')
    cy.get('.MuiBackdrop-root').should('contain', 'Enviando arquivo(s)')

    cy.get('.MuiTableBody-root > :nth-child(1) > :nth-child(1)', { timeout: 10000 }).should('contain', ArquivoUpload)
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 3600000 }).should('contain', 'Importação concluída 100%');
    cy.get('.MuiTableBody-root > > :nth-child(3)', { timeout: 3600000 }).should('contain', 'Resultados apurados 100%');

})

Cypress.Commands.add("exclusaoICMSNew", (NomeEmpresa) => {

    cy.wait(1000)
    cy.get(':nth-child(2) >>>>>>>>>').should('be.visible').then(Emps => {
        var listEmp = Emps.text()
        var isEmp = listEmp.includes(NomeEmpresa)

        console.log(listEmp)
        console.log(isEmp)

        if (isEmp) {

            cy.pesquisarTer(NomeEmpresa)

            cy.wait(1000)

            cy.get(loc.TERCEIROS.EMPRESA.REMOVER_PRIMEIRO).click({ force: true })
            cy.wait(1000)
            cy.get('button').contains('Apagar').click({ force: true })
            cy.wait(1000)

            // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('contain', 'Empresa removida com sucesso!')

            cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')

            cy.contains('Carregando empresas...').should('not.exist')

            cy.get(loc.TERCEIROS.MENU.III).click()


            // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('be.visible')

            cy.get('.MuiIconButton-label > .MuiSvgIcon-root').click()

            cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')



            cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

        }
    })
})

Cypress.Commands.add("deleteICMS", (deletartodas = false) => {

    cy.contains('Cadastrar')
    cy.wait(1000)


    cy.get('>>>>>>').should('be.visible').then(Emps => {

        cy.contains('Cadastrar')

        var listEmp = Emps.text()
        var isEmp = listEmp.includes('Sem empresas cadastradas')

        console.log(listEmp)
        console.log(isEmp)

        if (!isEmp) {
            console.log('APAGANDO')
            cy.get('[title="Apagar empresa"]:eq(0)').click({ force: true })
            cy.wait(1000)
            cy.get('button').contains('Apagar').click({ force: true })
            cy.wait(1000)

            cy.get('#modal-empresa ', { timeout: 36000 }).should('not.exist')
            // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('contain', 'Empresa removida com sucesso!')
            cy.get(loc.DIFAL.MENU.III).click()

            cy.get('.MuiTypography-root', { timeout: 36000 }).should('not.contain', 'Carregando empresa...')

            cy.contains('Carregando empresas...').should('not.exist')

            cy.contains('Cadastrar')

            // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('contain', 'Empresa removida com sucesso!')



            cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('not.exist')

            cy.wait(1000)

            cy.contains('Cadastrar')

            if (deletartodas) {
                cy.deleteTer(true)

            }
        }
    })
})

Cypress.Commands.add("deleteICMSAll", () => {
    cy.deleteICMS(true)

})

Cypress.Commands.add('pesquisarICMS', (NomeEmpresa) => {
    cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear().type(NomeEmpresa)
})

Cypress.Commands.add("selecioanarICMS", (NomeEmpresa) => {

    cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

    cy.wait(1000)
    cy.get(':nth-child(2) >>>>>>>>>').should('be.visible').then(Emps => {
        var listEmp = Emps.text()
        var isEmp = listEmp.includes(NomeEmpresa)

        console.log(listEmp)
        console.log(isEmp)

        if (isEmp) {

            cy.pesquisarICMS(NomeEmpresa)
            cy.wait(1000)
            cy.get('.header__cnpj:eq(0)').click()

            cy.wait(1000)

            cy.get(loc.TERCEIROS.MENU.III).click()


            cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

        }
    })
})

Cypress.Commands.add('crashLogoffICMS', () => {
    cy.wait(500)
    cy.url().then(URL => {

        var texto = URL

        if (texto == 'https://contribuicoesterceiros.e-auditoria.com.br/home') {

            cy.waitLoading()

            cy.visit(link, { timeout: 10000, force: true })

            cy.get(loc.LOGIN.USER, { timeout: 30000 }).should('be.visible').type(baseLogada)

            cy.get(loc.LOGIN.PASSWORD).type(senhaPadrao)
            cy.get(loc.LOGIN.BTN_LOGIN, { timeout: 50000 }).click({ force: true })

            cy.get('.title', { timeout: 30000 }).should('be.visible').should('contain', 'Todas as ferramentas que você precisa em um único lugar')
            cy.get(loc.MODULES.ERECUPERADOR, { timeout: 30000 }).should('be.visible').click({ force: true })
            cy.get('.block_tela', { timeout: 50000 }).should('not.exist')

            cy.waitLoading()

            cy.crashContrato()

            cy.acessoTerceiros()

        }


    })
})

Cypress.Commands.add('checkXLSSomaSelicICMS', (arquivo, row, column, sheet) => {

    cy.get(':nth-child(2)>>>>>>>>>>>>>>:eq(2)').then(result => {

        let credito = result.text()
        let totalSelic = credito.replace(/[^0-9.,]+/g, '')

        cy.checkXLSXter(arquivo, row, column, sheet, 'R$ ' + totalSelic)
    })
})

Cypress.Commands.add('crashCancelICMS', () => {
    cy.wait(1000)

    cy.get(':nth-child(1) ').should('be.visible').then(cancel => {
        var botoes = cancel.text()
        var btnCancelar = botoes.includes('Cancelar')

        if (btnCancelar) {
            cy.contains('Cancelar').click({ force: true })
        }
    })
})

Cypress.Commands.add("checkXLSXICMS", (arquivo, row, column, sheet, checkEqual) => {

    let fileWhere = 'cypress/downloads/'
    let file = fileWhere + arquivo

    cy.get(loc.MESSAGE.POPUP_DIFAL_2, { timeout: 360000 }).should('be.visible').should('contain', 'Relatório gerado com sucesso').then(() => {
        cy.task('readXlsx', { file, sheet }).then((rows) => {
            console.log(rows)
            expect(rows[row][column]).to.equals(checkEqual)

        })
    });
})

Cypress.Commands.add("checkExportICMS", (arquivo, row, column, sheet, checkEqual) => {

    let fileWhere = 'cypress/downloads/'
    let file = fileWhere + arquivo

    cy.get(loc.MESSAGE.POPUP_ICMS, { timeout: 360000 }).should('be.visible').should('contain', 'Histórico exportado com sucesso').then(() => {
        cy.task('readXlsx', { file, sheet }).then((rows) => {
            console.log(rows)
            expect(rows[row][column]).to.equals(checkEqual)

        })
    });
})

Cypress.Commands.add('crashContratoICMS', () => {

    cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

    cy.get(loc.TERCEIROS.EMPRESA.REMOVER_PRIMEIRO).click({ force: true })

    cy.wait(1000)
    cy.get('.MuiTypography-root').should('be.visible').then(contrato => {
        var botoes = contrato.text()
        var ccontrato = botoes.includes('Eu li e concordo com o contrato de uso')

        console.log(botoes)

        if (ccontrato) {
            cy.contains('Eu li e concordo com o contrato de uso').should('be.visible')
            cy.get('input').check()
            cy.contains('Aceito', { timeout: 36000 }).click()

            cy.contains('Contrato aceito com sucesso', { timeout: 36000 }).should('be.visible')

            cy.wait(2000)

        }

        else {
            cy.wait(1000)

            cy.contains('Cancelar').click()

        }


    })

});

Cypress.Commands.add('crashContratoICMS2', () => {

    cy.wait(1000)
    cy.get('.MuiTypography-root').should('be.visible').then(contrato => {
        var botoes = contrato.text()
        var ccontrato = botoes.includes('Eu li e concordo com o contrato de uso')

        console.log(botoes)

        if (ccontrato) {
            cy.contains('Eu li e concordo com o contrato de uso').should('be.visible')
            cy.get('input').check()
            cy.contains('Aceito',{timeout:10000}).click()


            cy.contains('Contrato aceito com sucesso',{timeout:10000})

            cy.wait(2000)


            cy.contains('Início').click()
            cy.wait(1000)
            cy.contains('Carregando empresas...').should('not.exist')
            cy.get('.header__cnpj:eq(0)').click()

            cy.wait(1000)

            cy.get(loc.TERCEIROS.MENU.III).click()

        }

        else {
            cy.wait(1000)
        }


    })

});

Cypress.Commands.add("exclusaoICMS", (NomeEmpresa) => {

    cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

    cy.wait(1000)

    cy.get(':nth-child(2) >>>>>>>>>').should('be.visible').then(Emps => {
        var listEmp = Emps.text()
        var isEmp = listEmp.includes(NomeEmpresa)

        console.log(listEmp)
        console.log(isEmp)

        if (isEmp) {

            cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()

            cy.wait(1000)

            cy.pesquisarTer(NomeEmpresa)

            cy.get(loc.TERCEIROS.EMPRESA.REMOVER_SEGUNDO).should('not.exist')

            cy.get(loc.TERCEIROS.EMPRESA.REMOVER_PRIMEIRO).click()

            cy.get('button').contains('Apagar').click({ force: true })
            cy.wait(1000)
            // cy.get(loc.MESSAGE.POPUP_TERCEIROS_2, { timeout: 36000 }).should('contain', 'Empresa removida com sucesso!')
            cy.wait(1000)
            cy.get(loc.TERCEIROS.EMPRESA.FILTRO_EMPRESAS).clear()
            cy.wait(1000)
        }
    })
})

Cypress.Commands.add('checkBaseCalculoICMS', (comp) => {

    for (let row = 1; row <= comp; row++) {

        cy.get('.MuiTableBody-root >> :nth-child(5):eq(' + row + ')').then(valIcms => {

            cy.get('.MuiTableBody-root >> :nth-child(7):eq(' + row + ')').then(baseIcmsCom => {

                cy.get('.MuiTableBody-root >> :nth-child(8):eq(' + row + ')').then(baseIcmsSem => {

                    let valIcmsTxt = valIcms.text()
                    let baseIcmsComTxt = baseIcmsCom.text()
                    let baseIcmsSemTxt = baseIcmsSem.text()

                    let valor = Number(valIcmsTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let baseCalCom = Number(baseIcmsComTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let baseCalSem = Number(baseIcmsSemTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = baseCalCom - valor

                    if (Calc < 0) {
                        Calc = 0.00
                    }

                    console.log(row + ' Valor ICMS : ' + valor)
                    console.log(row + ' Base Cálculo COFINS	: ' + baseCalCom)
                    console.log(row + ' Base Cálculo COFINS Sem ICMS : ' + baseCalSem)
                    console.log(row + ' Calculo: ' + Calc)

                    if (Calc != baseCalSem) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add('checkBaseCalculoICMS2', (comp) => {

    for (let row = 1; row <= comp; row++) {

        cy.get('.MuiTableBody-root >> :nth-child(6):eq(' + row + ')').then(valIcms => {

            cy.get('.MuiTableBody-root >> :nth-child(7):eq(' + row + ')').then(baseIcmsCom => {

                cy.get('.MuiTableBody-root >> :nth-child(8):eq(' + row + ')').then(baseIcmsSem => {

                    let valIcmsTxt = valIcms.text()
                    let baseIcmsComTxt = baseIcmsCom.text()
                    let baseIcmsSemTxt = baseIcmsSem.text()

                    let valor = Number(valIcmsTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let baseCalCom = Number(baseIcmsComTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let baseCalSem = Number(baseIcmsSemTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = baseCalCom - valor

                    if (Calc < 0) {
                        Calc = 0.00
                    }

                    console.log(row + ' Valor ICMS : ' + valor)
                    console.log(row + ' Base Cálculo COFINS	: ' + baseCalCom)
                    console.log(row + ' Base Cálculo COFINS Sem ICMS : ' + baseCalSem)
                    console.log(row + ' Calculo: ' + Calc)

                    if (Calc != baseCalSem) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add('checkBaseCalculoICMS3', (comp) => {

    for (let row = 1; row <= comp; row++) {

        cy.get('.MuiTableBody-root >> :nth-child(6):eq(' + row + ')').then(valIcms => {

            cy.get('.MuiTableBody-root >> :nth-child(8):eq(' + row + ')').then(baseIcmsCom => {

                cy.get('.MuiTableBody-root >> :nth-child(9):eq(' + row + ')').then(baseIcmsSem => {



                    let valIcmsTxt = valIcms.text()
                    let baseIcmsComTxt = baseIcmsCom.text()
                    let baseIcmsSemTxt = baseIcmsSem.text()

                    let valor = Number(valIcmsTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let baseCalCom = Number(baseIcmsComTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let baseCalSem = Number(baseIcmsSemTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = baseCalCom - valor

                    if (Calc < 0) {
                        Calc = 0.00
                    }

                    function decimalAdjust(type, value, exp) {
                        // Se exp é indefinido ou zero...
                        if (typeof exp === 'undefined' || +exp === 0) {
                            return Math[type](value);
                        }
                        value = +value;
                        exp = +exp;
                        // Se o valor não é um número ou o exp não é inteiro...
                        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                            return NaN;
                        }
                        // Transformando para string
                        value = value.toString().split('e');
                        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                        // Transformando de volta
                        value = value.toString().split('e');
                        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
                    }

                    if (!Math.round10) {
                        Math.round10 = function (value, exp) {
                            return decimalAdjust('round', value, exp);
                        };
                    }

                    let aCalc = Math.round10(Calc, -2)

                    console.log(row + ' Valor ICMS : ' + valor)
                    console.log(row + ' Base Cálculo COFINS	: ' + baseCalCom)
                    console.log(row + ' Base Cálculo COFINS Sem ICMS : ' + baseCalSem)
                    console.log(row + ' Calculo: ' + Calc)
                    console.log(row + ' Calculo Arredondado: ' + aCalc)

                    if (aCalc != baseCalSem) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add('crashCloseICMS', () => {
    cy.wait(1000)

    cy.get(':nth-child(2)>>>>>>>>>>>').should('be.visible').then(cancel => {
        var botoes = cancel.text()
        var btnFechar = botoes.includes('Consolidado')

        if (btnFechar) {
            cy.get('.MuiAppBar-positionFixed > .MuiToolbar-root > .MuiButtonBase-root').click({ force: true })
        }
    })
})

Cypress.Commands.add('checkSelicAtualPIS_COFINS_ICMS', (comp) => {


    for (let row = 1; row <= comp; row++) {

        cy.get(':nth-child(' + row + ') > :nth-child(7):eq(2)').then(CaixaVal => {

            cy.get(':nth-child(' + row + ') > :nth-child(9):eq(2)').then(CaixaSELIC => {

                cy.get(':nth-child(' + row + ') > :nth-child(10):eq(2)').then(CaixaATUAL => {


                    function decimalAdjust(type, value, exp) {
                        // Se exp é indefinido ou zero...
                        if (typeof exp === 'undefined' || +exp === 0) {
                            return Math[type](value);
                        }
                        value = +value;
                        exp = +exp;
                        // Se o valor não é um número ou o exp não é inteiro...
                        if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                            return NaN;
                        }
                        // Transformando para string
                        value = value.toString().split('e');
                        value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                        // Transformando de volta
                        value = value.toString().split('e');
                        return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
                    }

                    if (!Math.round10) {
                        Math.round10 = function (value, exp) {
                            return decimalAdjust('round', value, exp);
                        };
                    }

                    let valCreditoTxt = CaixaVal.text()
                    let SelicTxt = CaixaSELIC.text()
                    let valAtualTxt = CaixaATUAL.text()

                    let valCredito = Number(valCreditoTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let Selic = Number(SelicTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let valAtual = Number(valAtualTxt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    let Calc = Math.round10(valCredito * ((Selic * 0.01) + 1), -2)


                    console.log(row + ' Valor do Crédito: ' + valCredito)
                    console.log(row + ' SELIC : ' + Selic + ' %')
                    console.log(row + ' Crédito Atualizado SELIC: ' + valAtual)
                    console.log(row + ' Calculo: ' + Calc)

                    if (Calc != valAtual) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                    }
                })

            })
        })
    }
})

Cypress.Commands.add('checkCreditoApuradoICMSPis', () => {
    cy.get('.MuiTableRow-root > :nth-child(7)').then(credPis => {
        cy.get('.MuiTableRow-root > :nth-child(10)').then(credPisAtt => {

            let credPisVar = credPis.map((index, html) => Cypress.$(html).text()).get()
            let credPisAttVar = credPisAtt.map((index, html) => Cypress.$(html).text()).get()

            function decimalAdjust(type, value, exp) {
                // Se exp é indefinido ou zero...
                if (typeof exp === 'undefined' || +exp === 0) {
                    return Math[type](value);
                }
                value = +value;
                exp = +exp;
                // Se o valor não é um número ou o exp não é inteiro...
                if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                    return NaN;
                }
                // Transformando para string
                value = value.toString().split('e');
                value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                // Transformando de volta
                value = value.toString().split('e');
                return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
            }

            if (!Math.round10) {
                Math.round10 = function (value, exp) {
                    return decimalAdjust('round', value, exp);
                };
            }

            let credPisList = credPisVar.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
            let credPisCalc = Math.round10(credPisList, -2)

            let credPisAttList = credPisAttVar.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
            let credPisAttCalc = Math.round10(credPisAttList, -2)

            cy.get(':nth-child(3)>>>>>>>>>>>>:eq(0)').then(credApu => {
                cy.get(':nth-child(3)>>>>>>>>>>>>:eq(1)').then(credApuAtt => {
                    var apurado = credApu.text()
                    var apuradoAtt = credApuAtt.text()

                    let somaApurada = Number(apurado.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let somaAtualizada = Number(apuradoAtt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    console.log('Soma Apurada ' + somaApurada + ' & ' + 'Soma Apurada Calculada' + credPisCalc)
                    console.log('Soma Apurada Atualizada ' + somaAtualizada + ' & ' + 'Soma Apurada Atualizada Calculada ' + credPisAttCalc)


                    if (somaApurada != credPisCalc || somaAtualizada != credPisAttCalc) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                        cy.log('Total do crédito apurado: ' + somaApurada != credPisCalc)
                        cy.log('Total do crédito atualizado pela SELIC: ' + somaAtualizada != credPisAttCalc)
                    }

                })
            })
        })
    })
})

Cypress.Commands.add('checkCreditoApuradoICMSCofins', () => {
    cy.get('.MuiTableRow-root > :nth-child(7)').then(credPis => {
        cy.get('.MuiTableRow-root > :nth-child(10)').then(credPisAtt => {

            let credPisVar = credPis.map((index, html) => Cypress.$(html).text()).get()
            let credPisAttVar = credPisAtt.map((index, html) => Cypress.$(html).text()).get()

            function decimalAdjust(type, value, exp) {
                // Se exp é indefinido ou zero...
                if (typeof exp === 'undefined' || +exp === 0) {
                    return Math[type](value);
                }
                value = +value;
                exp = +exp;
                // Se o valor não é um número ou o exp não é inteiro...
                if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
                    return NaN;
                }
                // Transformando para string
                value = value.toString().split('e');
                value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
                // Transformando de volta
                value = value.toString().split('e');
                return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
            }

            if (!Math.round10) {
                Math.round10 = function (value, exp) {
                    return decimalAdjust('round', value, exp);
                };
            }

            let credPisList = credPisVar.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
            let credPisCalc = Math.round10(credPisList, -2)

            let credPisAttList = credPisAttVar.map(item => Number(item.replace(/[^0-9,]+/g, "").replace(',', '.'))).reduce((a, b) => a + b)
            let credPisAttCalc = Math.round10(credPisAttList, -2)

            cy.get(':nth-child(3)>>>>>>>>>>>>:eq(0)').then(credApu => {
                cy.get(':nth-child(3)>>>>>>>>>>>>:eq(1)').then(credApuAtt => {
                    var apurado = credApu.text()
                    var apuradoAtt = credApuAtt.text()

                    let somaApurada = Number(apurado.replace(/[^0-9,]+/g, "").replace(',', '.'))
                    let somaAtualizada = Number(apuradoAtt.replace(/[^0-9,]+/g, "").replace(',', '.'))

                    console.log('Soma Apurada ' + somaApurada + ' & ' + 'Soma Apurada Calculada' + credPisCalc)
                    console.log('Soma Apurada Atualizada ' + somaAtualizada + ' & ' + 'Soma Apurada Atualizada Calculada ' + credPisAttCalc)


                    if (somaApurada != credPisCalc || somaAtualizada != credPisAttCalc) {
                        cy.get('[data-row-key] > :nth-child(1)').should('contain', 'Erro de Calculo')
                        cy.log('Total do crédito apurado: ' + somaApurada != credPisCalc)
                        cy.log('Total do crédito atualizado pela SELIC: ' + somaAtualizada != credPisAttCalc)
                    }

                })
            })
        })
    })
})

Cypress.Commands.add('checkConsolidadoICMS', (ArquivoConsolidado, tipoPlanilha) => {

    let pag

    if (tipoPlanilha == 'PIS') {
        pag = 'Consolidado PIS'
    }

    else if (tipoPlanilha == 'COFINS') {
        pag = 'Consolidado COFINS'
    }


    cy.get(':nth-child(3)>>>>>>>>>>>>:eq(0)').then(credApu => {
        cy.get(':nth-child(3)>>>>>>>>>>>>:eq(1)').then(credApuAtt => {
            var apurado = credApu.text()
            var apuradoAtt = credApuAtt.text()

            let somaApurada = 'R$ ' + apurado.replace(/[^0-9,]+/g, "")
            let somaAtualizada = 'R$ ' + apuradoAtt.replace(/[^0-9,]+/g, "")

            // let  = apu.replace('.', ',')
            // let  = att.replace('.', ',')


            let valorAtualizado

            if (somaAtualizada == 'R$ 0,00') {
                valorAtualizado = '-'
            }

            else {
                valorAtualizado = somaAtualizada
            }





            cy.get('.MuiAppBar-positionFixed > .MuiToolbar-root > .MuiTypography-root').should('contain', 'Resultado - ')

            cy.contains('Carregando...').should('not.exist')

            cy.contains(' Nenhum resultado encontrado').should('not.exist')

            cy.get('.MuiPaper-root > .MuiTableContainer-root > .MuiTable-root > .MuiTableBody-root > :nth-child(1) > :nth-child(5):eq(1)').should('be.visible')

            cy.get(':nth-child(1) > .MuiButton-label').click()


            cy.checkXLSXICMS(ArquivoConsolidado, 3, '__EMPTY', pag, somaApurada)

            cy.checkXLSXICMS(ArquivoConsolidado, 4, '__EMPTY', pag, valorAtualizado)

        })
    })
})

Cypress.Commands.add('loadingCircular', () => {
    cy.get('.MuiCircularProgress-svg').should('exist')
    cy.get('.MuiCircularProgress-svg').should('not.exist')
})

Cypress.Commands.add('selectCFOP_ICMS', (buscar) => {
    cy.get(loc.ICMS.ABAS_EMPRESA.CONFIGURACOES).click()
    cy.loadingCircular()
    cy.get(loc.ICMS.CONFIGURACOES.INPUT_BUSCA).type(buscar)
    cy.loadingCircular()
    cy.get(loc.ICMS.CONFIGURACOES.SWITCH_1).should('have.css', 'color', 'rgb(250, 250, 250)') //DESMARCADO
    cy.get(loc.ICMS.CONFIGURACOES.SWITCH_1).click()
    cy.loadingCircular()
    cy.get(loc.ICMS.CONFIGURACOES.SWITCH_1).should('have.css', 'color', 'rgb(58, 184, 122)') //MARCADO
    cy.get(loc.ICMS.CONFIGURACOES.INPUT_BUSCA).clear()
    cy.get(loc.ICMS.ABAS_EMPRESA.DOCUMENTOS).click()
    cy.wait(1000)
})

