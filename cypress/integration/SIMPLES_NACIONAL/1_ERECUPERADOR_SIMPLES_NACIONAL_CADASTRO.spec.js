/// <reference types="cypress"/>
import loc from '../../support/locators'

describe('1_ERECUPERADOR_SIMPLES_NACIONAL_CADASTRO', () => {

    before(() => {
        cy.login(1)
        cy.PrimeiroRegistro()
    });

    beforeEach(() => {
        cy.wait(1000)
        cy.crashLogoff(1)
        cy.crashCancel()
        cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS).click({ timeout: 5000 })
    });

    after(() => {
        cy.crashCancel()
        cy.deleteAll()
    });

    describe('Novo Processo de Recuperação', () => {

        it('Teste não deve cadastrar uma empresa sem dados', () => {


            cy.get(loc.ERECUPERADOR_BOTOES.MENU.NOVO, { timeout: 5000 }).should('be.visible')
            cy.wait(500)
            cy.get(loc.ERECUPERADOR_BOTOES.MENU.NOVO, { timeout: 5000 }).click({ force: true })
            cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR, { timeout: 5000 }).should('be.disabled')
            cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type('Teste Vazio')
            cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).should('be.disabled');
            cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).clear()
            cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type('13331616000197')
            cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).should('be.disabled');
            cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CANCELAR).click({ timeout: 5000 })
            cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS, { timeout: 5000 }).should('be.visible').click()

        });

        it('Teste não deve cadastrar uma empresa com cnpj inválido', () => {
            cy.get(loc.ERECUPERADOR_BOTOES.MENU.NOVO).click({ timeout: 5000 })
            cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type('Teste CNPJ Inválido')
            cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type('34.234.234/2342-34')
            cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).should('be.disabled')

            cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CANCELAR).click({ timeout: 5000 })
        });

        it('REC-74 - ESPERANDO CORRECAO - Teste não deve cadastrar uma empresa com Inscrição Estadual inválida', () => {
            cy.get(loc.ERECUPERADOR_BOTOES.MENU.NOVO).click({ timeout: 5000 })
            cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.TXT_NOME).type('Teste inscrição estadual inválido')
            cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_CNPJ).type('74.520.783/0001-70')
            cy.get(loc.ERECUPERADOR_CAMPOS.CADASTRO.NUM_IE).type('74.520.783/0001-70')
            cy.get(loc.ERECUPERADOR_BOTOES.CADASTRO.CADASTRAR).click({ timeout: 5000 })
            cy.get('ERRO_ARTIFICAL').click()

        });

    });

});