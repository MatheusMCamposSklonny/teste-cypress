/// <reference types="cypress"/>
import loc from '../../support/locators'

describe('0_ERRO_DE_CONTROLE', () => {

    before(() => {
        cy.login(1)
        cy.PrimeiroRegistro()
    });

    beforeEach(() => {
        cy.wait(1000)
        cy.crashLogoff(1)
        cy.crashCancel()
        cy.get(loc.ERECUPERADOR_BOTOES.MENU.TODAS).click({ timeout: 5000 })
    });

    after(() => {
        cy.crashCancel()
        cy.deleteAll()
    });

    describe('Erro de controle', () => {

        it(' Erro de controle para proteger primeiro teste de erro gerado pelo inicio do .env', () => {
            
            cy.criarEmpresa('teste de notificação', CNPJEmpresa)
            cy.filtroEmpresas('teste de notificação')
            cy.get('ERRO_ARTIFICAL').click()

        });

    });

});